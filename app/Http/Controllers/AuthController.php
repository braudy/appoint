<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 5/14/2016
 * Time: 2:36 AM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers;

    public function showRegistrationFormDoctor(){
        return view('auth/doctor/register');
    }

    public function showRegistrationFormPatient(){
        return view('auth.register');
    }

    public function showLoginForm(){
        return view('auth.login');
    }

    public function login(Request $request){
        $rules = array(
            'username'    => 'required',
            'password' => 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('login')
                ->withErrors($validator)
                ->withInput($request->except('password'));
        } else {

            $userdata = array(
                'username'     => $request->get('username'),
                'password'  => $request->get('password')
            );

            if (Auth::attempt($userdata)) {
                return redirect('/dashboard');
            } else {
                flash()->error("Invalid username or password!");
                return redirect('/login');
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}