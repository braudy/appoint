<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedSched\Models\Queue;
use App\MedSched\Models\schedule;
use App\MedSched\Models\Patient;
use App\MedSched\Models\DoctorSettings;


use App\Http\Requests;
use Auth;

class DashboardController extends Controller
{
    public function showDashboard(){
    	$doctor_id = 0;
    	$patient_id = 0;
    	$id = 0;

        $timeSlotsTaken = array();
        $timeSlots = array();
        if(Auth::user()->person->doctor != null){
            $doctor_id = Auth::user()->person()->first()->doctor()->first()->person_id;
            $id = $doctor_id;

        $settings = DoctorSettings::where("settings.doctor_id", "=", $id)->get()->first();

        $schedArray = explode("|", $settings->schedule);
        $amLimit = $settings->amLimit;
        $pmLimit = $settings->pmLimit;

        $days = explode(",", $schedArray[1]);
        $time = explode(",", $schedArray[0]);

        $time1 = explode(":", $time[0]);
        $time2 = explode(":", $time[1]);
        $amTimeDiff = intval($time2[0]) - intval($time1[0]);
        $amTimeInterval = ($amTimeDiff*60)/$settings->amLimit;

        $limiterAM = 60/$amTimeInterval;

        
        $timeStart = date('g:i', strtotime($time[0]));

        for($i = 0; $i < $amLimit; $i++){
            $timeSlot = $timeStart .' - '. date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            $timeStart = date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            array_push($timeSlots, $timeSlot);
        }

        $timeStart = date('g:i', strtotime($time[2]));
        for($i = 0; $i < $pmLimit; $i++){
            $timeSlot = $timeStart .' - '. date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            $timeStart = date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            array_push($timeSlots, $timeSlot);
        }

        $datenow = "2016-12-05";

        $schedule = schedule::where('date','=',$datenow)->get();

        for ($i=0; $i < sizeof($schedule); $i++) { 
            array_push($timeSlotsTaken, $schedule[$i]['timeSlot']);
        }


        $slotsDiff = array_diff($timeSlots, $timeSlotsTaken);

        $slotsDiffClean = array();

        foreach ($slotsDiff as $slot) { 
            array_push($slotsDiffClean, $slot);
        }

        $timeSlots = $slotsDiffClean;
        }
        else{
            $patient_id = Auth::user()->person_id;
            $id = $patient_id;
        }


    	$doctorData = Queue::join('persons', 'queues.doctor_id', '=', 'persons.id')
    						->join('users', 'queues.doctor_id', '=', 'users.person_id')
    						->orderBy('queues.created_at','asc')
    						->where('queues.patient_id','=',$patient_id)
                            ->where('queues.status', '=', 'pending')
    						->get()->toArray();

    	$patientData = Queue::join('persons', 'queues.patient_id', '=', 'persons.id')
    						->join('users', 'queues.patient_id', '=', 'users.person_id')
    						->orderBy('queues.created_at','asc')
    						->where('queues.doctor_id','=',$doctor_id)
                            ->where('queues.status', '=', 'pending')
    						->get()->toArray();

        $patientQueues = Queue::where('queues.doctor_id', '=', $doctor_id)->where('queues.status', '=', 'pending')->get();
        $doctorQueues = Queue::where('queues.patient_id', '=', $patient_id)->where('queues.status', '=', 'pending')->get();

        $doctorSched = Schedule::where('schedules.doctor_id','=', $doctor_id)->get();
        $patientSched = Schedule::where('schedules.patient_id','=', $patient_id)->get();

        $doctorSchedData = Schedule::join('persons', 'schedules.patient_id', '=', 'persons.id')
                            ->where('schedules.doctor_id', '=', $doctor_id)->get();

        $patientSchedData = Schedule::join('persons', 'schedules.doctor_id', '=', 'persons.id')
                            ->where('schedules.patient_id', '=', $patient_id)->get();

        $patientsList = Patient::join('persons', 'patients.person_id', '=', 'persons.id')
        ->get()->toArray();

        // return $patientsList;


        
        return view('general.dashboard', compact('patientsList','doctorData','patientData','patientQueues','doctorQueues','timeSlots','doctorSched','patientSched','doctorSchedData','patientSchedData'));
    }
}
