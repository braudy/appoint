<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{


    /**
     * PagesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth',['except'=>['showHome','showProfile','showFind']]);
    }

    public function showHome(){
        return view('home.index');
    }

    public function showProfile(){
        return view('doctor.profile');
    }

    public function showDashboard(){
        return view('general.dashboard');
    }

    public function showSettings(){
        return view('general.settings');
    }

    public function showFind(){
        return view('doctor.find');
    }
}
