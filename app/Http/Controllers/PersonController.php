<?php

namespace App\Http\Controllers;

use App\MedSched\Classes\DoctorReporter;
use App\MedSched\Classes\PatientReporter;
use App\MedSched\Classes\PersonReporter;
use App\MedSched\Models\DoctorSettings;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Validator;
use Auth;


class PersonController extends Controller
{

    /**
     * @var PersonReporter
     */
    private $person;

    public function __construct(PersonReporter $person)
    {
        $this->middleware('db.checker');
        $this->person = $person;
    }

    public function storeDoctor(Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'required|min:3',
            'password' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            flash()->error("Invalid username or password!");
            return redirect()->back()->withInput();
        }

        $newUser = new DoctorReporter();

        $this->person->createPerson($request,$newUser);
        flash()->success('Successfully registered!');
        return redirect()->back();
    }

  

    public function storePatient(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:3',
            'password' => 'required|min:3'
        ]);

        if ($validator->fails())
        {
            flash()->error("Invalid username or password!");
            return redirect()->back()->withInput();
        }

        $newUser = new PatientReporter();

        $this->person->createPerson($request,$newUser);

        flash()->success('Successfully registered!');
        return redirect()->back();
    }

    public function displaySettings($id){
        $settings = DoctorSettings::findOrFail($id);

        return $settings;
    }
}
