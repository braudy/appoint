<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MedSched\Models\Doctor;
use App\MedSched\Models\Person;
use App\MedSched\Models\Queue;
use App\MedSched\Models\DoctorReview;
use App\MedSched\Models\DoctorSettings;
use Auth;
use View;

class ProfileController extends Controller
{
    public function showProfile($id)
    {
        $data = Doctor::join('persons', 'doctors.person_id', '=', 'persons.id')
                        ->join('users', 'doctors.person_id', '=', 'users.person_id')
                        ->orderBy('persons.created_at', 'desc')
                        ->where('doctors.person_id', '=', $id)
                        ->first();

        $reviews = DoctorReview::join('persons','reviews.patient_id', '=', 'persons.id')
                                ->join('users','reviews.patient_id', '=', 'person_id')
                                ->get()->toArray();

        $ratings = DoctorReview::where('reviews.doctor_id','=',$id)->get();

        $totalRating = 0;

        if(sizeof($ratings) != 0){
            for($u = 0; $u < sizeof($ratings); $u++){
                $totalRating += $ratings[$u]['rating'];
            }

            $doctor = Doctor::findOrFail($id)->get()->first();
            $doctor->rating = $totalRating;
            $doctor->save();

            $totalRating = $totalRating/sizeof($ratings);
        }
        $queues = Queue::where('queues.doctor_id', '=', $id)
                ->where('queues.status','=', 'pending')->get()->toArray();
        $queues_all = Queue::where('queues.doctor_id', '=', $id)->get()->toArray();
        $settings = DoctorSettings::where("settings.doctor_id", "=", $id)->get()->first();

        $schedArray = explode("|", $settings->schedule);

        $days = explode(",", $schedArray[1]);
        $time = explode(",", $schedArray[0]);

        $amLimit = $settings->amLimit;
        $pmLimit = $settings->pmLimit;
        
        $time1 = explode(":", $time[0]);
        $time2 = explode(":", $time[1]);
        $amTimeDiff = intval($time2[0]) - intval($time1[0]);
        $amTimeInterval = ($amTimeDiff*60)/$settings->amLimit;

        $limiterAM = 60/$amTimeInterval;

        $timeSlots= array();
        $timeStart = date('g:i', strtotime($time[0]));

        for($i = 0; $i < $amLimit; $i++){
            $slot = $timeStart .' - '. date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            $timeStart = date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            array_push($timeSlots, $slot);
        }

        $timeStart = date('g:i', strtotime($time[2]));
        for($i = 0; $i < $pmLimit; $i++){
            $slot = $timeStart .' - '. date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            $timeStart = date('g:i', strtotime($timeStart."+".$amTimeInterval."minutes"));
            array_push($timeSlots, $slot);
        }
    

        $header = $time1[0];
    
        date_default_timezone_set('Asia/Hong_Kong');

        $amQueue = Queue::whereRaw('date(created_at) = ?', [date('Y-m-d')])->get()->count();

        $headerTime = Queue::where('queues.doctor_id', '=', $id)
                    ->where('queues.status','=','pending')->get()->last();


        $firstTimeSlot = date('g:i') .' - '. date('g:i', strtotime("+".$amTimeInterval."minutes"));

        if($headerTime != null){
            $startPrevTime = explode(' - ', $headerTime->timeSlot)[1];
            $nextTime = date('g:i', strtotime($startPrevTime."+".$amTimeInterval." minutes"));
        }else{
            $startPrevTime = date('g:i');
        }
    
        
        // for($i=0; $i<($settings->amLimit/$limiterAM); $i++){
        //     for($j=1; $j<= $limiterAM; $j++){
        //         if($j != 3){
        //             $slot = $startPrevTime. " - " . $header . ":" . $amTimeInterval*$j;
        //             $startPrevTime = $header . ":" . $amTimeInterval*$j;
        //         }else
        //         {
        //             $header++;
        //             $slot = $startPrevTime . " - " . $header.":00";
        //             $startPrevTime = $header.":00";
        //         }
        //         array_push($timeSlotsAM, $slot);
        //     }
        // }

        $estimates = array();
        for($o=2;$o<sizeof($queues);$o++){
            $datetime1 = strtotime(date('g:i'));
            $datetime2 = strtotime(explode(' - ', $queues[$o]['timeSlot'])[1]);
            $secs = $this->secondsToTime($datetime2 - $datetime1);// == return sec in difference
            array_push($estimates,$secs);
        }

        $datetime1 = strtotime(date('g:i'));
        $datetime2 = strtotime('11:38');
        $secs = $this->secondsToTime($datetime2 - $datetime1);// == return sec in difference
        $hours = $secs / 3600;
     
        // return $timeSlots;
        return view('doctor.profile', compact('data','reviews','queues','queues_all','settings',"days","time","amQueue",'timeSlotsAM','amTimeInterval','startPrevTime','nextTime','firstTimeSlot','estimates','timeSlots'));
    }

    function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        if($seconds >= 3600)
            return $dtF->diff($dtT)->format('%h hours, %i minutes');
        else
            return $dtF->diff($dtT)->format('%i minutes');
    }

    public function showSettings($id)
    {
        $data = Doctor::join('persons', 'doctors.person_id', '=', 'persons.id')
                        ->join('users', 'doctors.person_id', '=', 'users.person_id')
                        ->orderBy('persons.created_at', 'desc')
                        ->where('doctors.person_id', '=', $id)
                        ->first();

        $settings = DoctorSettings::where("settings.doctor_id", "=", $id)->get()->first();

        $schedArray = explode("|", $settings->schedule);
    

        $days = explode(",", $schedArray[1]);
        $time = explode(",", $schedArray[0]);

        
        return view('general.settings', compact('data','settings','days',"time"));
    }

    public function submitReview(Request $request)
    {
        $user_id = Auth::user()->id;
        $docrev = new DoctorReview;

        $docrev->doctor_id = $request->doctor_id;
        $docrev->patient_id = $user_id;
        $docrev->review_description = $request->review;
        $docrev->rating = $request->rating;

        $docrev->save();


        return redirect()->back();
    }

    public function getReviews($id){
        $data = DoctorReview::where('doctor_id', '=', $id)->first();

        return $data;
    }
}