<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MedSched\Models\Queue;
use App\MedSched\Models\User;
use App\MedSched\Models\Patient;
use Auth;
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class QueueController extends Controller
{

    public function addToQueue(Request $request){
      $person_id = Auth::user()->person_id;
      $person_contact = Auth::user()->contact;
      $doctor_id = $request->doctor_id;

      $queue = Queue::create(['patient_id'=>$person_id, 'doctor_id'=>$doctor_id,'timeSlot'=>$request->timeSlot,'queueNumber'=>$request->queueNumber, 'status'=>'pending']);

      $this->sendMessage($person_contact,$queue->queueNumber,$queue->timeSlot);

      return redirect()->back();
    }

    public function addToQueuePortable(Request $request){
      $person_id = $request->patient_id;
      $patient = Patient::join('users','patients.person_id','=','users.person_id')->
      where('patients.person_id', '=',$person_id)->get()->first()->toArray();
      $person_contact = $patient['contact'];
      $doctor_id = $request->doctor_id;

      $queue = Queue::create(['patient_id'=>$person_id, 'doctor_id'=>$doctor_id,'timeSlot'=>$request->timeSlot,'queueNumber'=>$request->queueNumber, 'status'=>'pending']);

      $this->sendMessage($person_contact,$queue->queueNumber,$queue->timeSlot);

      return $patient;
    }

    public function sendMessage($phone, $queueNumber, $timeSlot)
  {
      // Your Account SID and Auth Token from twilio.com/console
    $sid = 'AC39b42ad29dcabe6df1f69b5594f5c2f5';
    $token = '595896ed8d01e921e2efb38e5bac156b';
    $client = new Client($sid, $token);

    $message = 
      'Your queue number is '.$queueNumber.' and will be served around '.$timeSlot.'..';

    // Use the client to do fun stuff like send text messages!
    $client->messages->create(
      // the number you'd like to send the message to
      '+63'.substr($phone, -10),
      array(
      // A Twilio phone number you purchased at twilio.com/console
      'from' => '+18133443608',
      // the body of the text message you'd like to send
      'body' => $message,
      )
    );
  }

  public function sendBulk($prev, $phones){
    $sid = 'AC39b42ad29dcabe6df1f69b5594f5c2f5';
    $token = '595896ed8d01e921e2efb38e5bac156b';
    $client = new Client($sid, $token);
    for($i = 0;$i < sizeof($phones);$i++){

    if($i == 0)
      $message = 'We just finished serving # '.$prev.'. You will be the next one to be served';
    if($i == 1)
      $message = 'We just finished serving #'.$prev.'. Please come to the clinic..';

    $client->messages->create(
      // the number you'd like to send the message to
      '+63'.substr($phones[$i], -10),
      array(
      // A Twilio phone number you purchased at twilio.com/console
      'from' => '+18133443608',
      // the body of the text message you'd like to send
      'body' => $message,
      )
    );
    }
  }

    

    public function removeFromQueue(Request $request){
      $queue_id = $request->id;
      $queue = Queue::findOrFail($queue_id);
      $queue->delete();
      return redirect()->back();
    }

    public function updateQueue(Request $request){
      date_default_timezone_set('Asia/Hong_Kong');
      $queue_id = $request->id;
      
      $queue = Queue::findOrFail($queue_id);

      $queue->status = "success";
      $queue->save();

      $queueList = Queue::where('queues.id','>',$queue_id)->where('queues.status','=','pending')->get();
      $queueOld = Queue::join('persons', 'queues.patient_id', '=', 'persons.id')
                ->join('users', 'queues.patient_id', '=', 'users.person_id')
                ->orderBy('queues.created_at','asc')
                ->where('queues.id','>',$queue_id)
                ->where('queues.status', '=', 'pending')
                ->get()->toArray();

      $test = array();
      $prev = date('g:i');

      for($i = 0; $i < sizeof($queueList); $i++){
        $curr_id = $queueList[$i]['id'];
        
        $queue_to_update = Queue::findOrFail($curr_id);
        $timeSlotOld = explode(' - ', $queue_to_update->timeSlot);

        $timeDif = $this->secondsToTime((strtotime($timeSlotOld[1]) - strtotime($timeSlotOld[0])));
  

        if($i==0){
          $next = date('g:i', strtotime($prev."+".$timeDif." minutes"));
        }else{
          $prev = $next;
          $next = date('g:i', strtotime($prev."+".$timeDif." minutes"));
        }

        $queue_to_update->timeSlot = $prev .' - '.$next;
        $queue_to_update->timeSlot = date('g:i').' - '.date('g:i', strtotime($timeSlotOld[1]."+".$timeDif." minutes"));
        $queue_to_update->save();
      }
      $phone1 = '09269139486';
      $phone2 = '09269139486';
      $phones = array();
      if(sizeof($queueOld) == 2)
        $phone1 = $queueOld[1]['contact'];
      if(sizeof($queueOld) == 3)
        $phone2 = $queueOld[2]['contact'];

      array_push($phones, $phone1);
      array_push($phones, $phone2);
      $this->sendBulk($queue->queueNumber,$phones);

      // return $queueOld;

      // $queue_to_update = Queue::findOrFail($queue_id_to_update);
      // $queue_id_to_update = $request->id+1;
      // $timeSlotOld = explode(' - ', $queue->timeSlot);
      // $time1 = explode(':', $timeSlotOld[0])[1];
      // $time2 = explode(':', $timeSlotOld[1])[1];
      // $timeDif = intval($time2) - intval($time1);
      // $queue_to_update->timeSlot = date('g:i').' - '.date('g:i', strtotime($timeSlotOld[1]."+".$timeDif." minutes"));
      // $queue_to_update->save();
      return redirect()->back();
    }

    

    function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
      
        return $dtF->diff($dtT)->format('%i');
    }



    public function getCount(){
      return Queue::count();
    }
}
