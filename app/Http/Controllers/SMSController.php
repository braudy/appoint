<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

use App\Http\Requests;

class SMSController extends Controller
{
	public function sendMessage(Request $requests)
	{
			// Your Account SID and Auth Token from twilio.com/console
		$sid = 'AC39b42ad29dcabe6df1f69b5594f5c2f5';
		$token = '595896ed8d01e921e2efb38e5bac156b';
		$client = new Client($sid, $token);

		// Use the client to do fun stuff like send text messages!
		$client->messages->create(
			// the number you'd like to send the message to
			'+639269139486',
			array(
			// A Twilio phone number you purchased at twilio.com/console
			'from' => '+18133443608',
			// the body of the text message you'd like to send
			'body' => $requests->message,
			)
		);
	}

	public function sendBulk(Request $requests){
		$sid = 'AC39b42ad29dcabe6df1f69b5594f5c2f5';
		$token = '595896ed8d01e921e2efb38e5bac156b';
		$client = new Client($sid, $token);

		// Use the client to do fun stuff like send text messages!
		$client->messages->create(
			// the number you'd like to send the message to
			'+639269139486',
			array(
			// A Twilio phone number you purchased at twilio.com/console
			'from' => '+18133443608',
			// the body of the text message you'd like to send
			'body' => $requests->message,
			)
		);
	}

	public function goTo(){
		return view('test');
	}
}
