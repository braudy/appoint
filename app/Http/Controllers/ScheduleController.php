<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedSched\Models\Schedule;
use App\MedSched\Models\User;
use App\MedSched\Models\Notification;
use App\Http\Requests;
use Auth;
use Twilio\Rest\Client;

class ScheduleController extends Controller
{
    public function addSchedule(Request $request)
    {
    	$person_contact = Auth::user()->contact;

    	$schedule = new Schedule();
    	$schedule->timeSlot = $request->time;
    	$schedule->date = date('F j, Y',strtotime($request->date));
    	$schedule->status = "pending";
    	$schedule->doctor_id = $request->doctor_id;
    	$schedule->patient_id = $request->patient_id;

    	$doctorContact = User::where('person_id','=',$request->doctor_id)->get()->first();

    	$schedule->save();

    	$messagePatient = 
	      "You have successfully booked an appointment! Please check the patient dashboard for more information. Thank you";

        $messageDoctorNot = 
	      "A patient booked an appointment! Click here to check..";

	    $messageDoctor = 
	      "A patient booked an appointment! Please check the doctor's dashboard.. Thank you";

    	

    	$this->sendMessage($person_contact,$messagePatient);

    	$this->sendMessage($doctorContact->contact,$messageDoctor);

    	return "Added";
    }

    public function updateSchedule(Request $request)
    {
    	$schedule = Schedule::findOrFail($request->sched_id)->get()->first();
    	$schedule->timeSlot = $request->time;
    	$schedule->date = date('F j, Y',strtotime($request->date));

    	$schedule->save();

    	$contact = User::where('person_id','=',$request->patient_id)->get()->first();

    	$message = 
	      "Your appointment has been rescheduled due to not being able to come at the expected time.. Please visit the clinic or the patient dashboard. Thank you";

    	$this->sendMessage($contact->contact, $message);

    	return $schedule;
    }

    public function deleteSchedule(Request $request)
    {
      $sched_id = $request->sched_id;
      $schedule = Schedule::findOrFail($sched_id);
      $schedule->delete();
      return "Deleted";
    }

     public function sendMessage($phone, $message)
	  {
	      // Your Account SID and Auth Token from twilio.com/console
	    $sid = 'AC39b42ad29dcabe6df1f69b5594f5c2f5';
	    $token = '595896ed8d01e921e2efb38e5bac156b';
	    $client = new Client($sid, $token);

	    // Use the client to do fun stuff like send text messages!
	    $client->messages->create(
	      // the number you'd like to send the message to
	      '+63'.substr($phone, -10),
	      array(
	      // A Twilio phone number you purchased at twilio.com/console
	      'from' => '+18133443608',
	      // the body of the text message you'd like to send
	      'body' => $message,
	      )
	    );
	  }

  public function getSchedule(Request $request)
    {
        $date = date('F j, Y',strtotime($request->date));

        $data = Schedule::join('persons','schedules.patient_id','=','persons.id')
        ->where("schedules.date",'LIKE','%'.$date.'%')->get()->toArray();

        return $data;
    }
}
