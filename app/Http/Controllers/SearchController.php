<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\MedSched\Models\Doctor;

class SearchController extends Controller
{
    public function makeSearch(Request $request){
    $key = $request->searchQuery;
    $key2 = $request->specialization;

    $data = Doctor::join('persons', 'doctors.person_id', '=', 'persons.id')
             ->join('users', 'doctors.person_id', '=', 'users.person_id')
             ->orderBy('persons.created_at', 'desc')
             ->where('doctors.specialization', 'LIKE', '%'.$key2.'%')
             ->where('persons.firstname', 'LIKE', '%'.$key.'%')
             ->orWhere('persons.lastname', 'LIKE', '%'.$key.'%')
             ->orWhere('doctors.clinic', 'LIKE', '%'.$key.'%')
             ->orWhere('persons.address', 'LIKE', '%'.$key.'%')
             ->get()
             ->toArray();
                         
      return $data;
    }

    public function displayAll(){
      $data = Doctor::join('persons', 'doctors.person_id', '=', 'persons.id')
                         ->orderBy('persons.created_at', 'desc')
                         ->get()
                         ->toArray();
      return $data;
    }

}
