<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\MedSched\Models\Doctor;
use App\MedSched\Models\DoctorSettings;
use App\MedSched\Models\Person;
use Illuminate\Support\Facades\Redirect;
use Auth;

class UpdateController extends Controller
{
  public function updateDoctorInfo(Request $request){
    $user = Auth::user();
    $person = Auth::user()->person()->first();
    $doctor = Auth::user()->person()->first()->doctor()->first();
    

    $user->email_add = $request->get('email');
    $user->contact = $request->get('contact');
    $user->save();

    $person->firstname = $request->get('firstname');
    $person->lastname = $request->get('lastname');
    $person->address = $request->get('home_address');
    $person->save();

    $doctor->clinic = $request->get('clinic_address');
    $doctor->specialization = $request->get('specialization');
    $doctor->description = $request->get('description');
    $doctor->save();

    return redirect()->back();
  }

  public function updateDoctorSched(Request $request){
    $url = $request->URL;
    $sched = $request->sched;

    $doctor = Auth::user()->person()->first()->doctor()->first();
    $count = DoctorSettings::where("settings.doctor_id", "=", $doctor->person_id)->count();

    if($count == 1){
        $settings = DoctorSettings::where("settings.doctor_id", "=", $doctor->person_id)->get()->first();
        $settings->schedule = $sched;
    }else{
        $settings = new DoctorSettings();
        $settings->schedule = $sched;
        $settings->doctor_id = $doctor->person_id;
    }

    $settings->save();


    return $settings;
  }

  public function updateDoctorConfig(Request $request){
    $am = $request->am;
    $pm = $request->pm;

    $doctor = Auth::user()->person()->first()->doctor()->first();
    $count = DoctorSettings::where("settings.doctor_id", "=", $doctor->person_id)->count();

    if($count == 1){
        $settings = DoctorSettings::where("settings.doctor_id", "=", $doctor->person_id)->get()->first();
        $settings->amLimit = $am;
        $settings->pmLimit = $pm;
    }else{
        $settings = new DoctorSettings();
        $settings->amLimit = $am;
        $settings->pmLimit = $pm;
        $settings->doctor_id = $doctor->person_id;
    }

    $settings->save();
    
    return $settings;
  }
}
