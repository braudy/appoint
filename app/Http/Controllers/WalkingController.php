<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MedSched\Interfaces\UserRepositoryInterface;
use App\MedSched\Models\Patient;
use App\MedSched\Classes\PersonCreator;
use App\Http\Requests;

class WalkingController extends Controller
{
    public function addNewPatient(Request $request)
    {
    	$personCreator = new PersonCreator();
        $person = $personCreator->createPerson($request);

        Patient::create(['person_id' => $person->id]);

        return $person->id;
    }

    public function viewAllPatients(Request $request){
    	$data = Patient::join('persons', 'patients.person_id', '=', 'persons.id')
    	->where('persons.firstname','LIKE','%'.$request->search.'%')
    	->orWhere('persons.lastname','LIKE','%'.$request->search.'%')
    	->limit(10)->get()->toArray();

    	return $data;
    }

    public function viewAll(){
    	$data = Patient::join('persons', 'patients.person_id', '=', 'persons.id')
    	->get()->toArray();

    	return $data;
    }
}
