<?php

namespace App\Http\Middleware;

use App\MedSched\Models\User;
use Closure;

class CheckDuplicate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $checkParameters = array('username','email_add','contact');
    protected $stringFiller = " already exist!";
    protected $errorString;
    protected $errors = array();
    protected $count;
    protected $flag = false;

    private function checkIfExist($request){
        foreach ($this->checkParameters as $value){
            if($request->has($value)){
                if(User::where($value,$request->get($value))->lists($value)->first() != null){
                    if($value == 'email_add'){
                        $value = 'Email Address';
                    }
                    array_push($this->errors, ucfirst($value) . $this->stringFiller);
                    $this->flag = true;
                }
            }
        }

        return $this->flag;
    }

    private function errorToString(){
        foreach ($this->errors as $value){

            $this->errorString = $this->errorString . $value . "\n";
        }
        return $this->errorString;
    }

    public function handle($request, Closure $next)
    {
        if($this->checkIfExist($request)){
            flash()->error($this->errorToString());
            return redirect()->back()->withInput();
        }
        return $next($request);
    }
}
