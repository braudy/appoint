<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function (){
    return view('home.index');
});

Route::group(['middleware' => ['web']], function () {

    Route::get('/login', 'AuthController@showLoginForm');
    Route::post('/login', 'AuthController@login');
    Route::get('/logout', 'AuthController@logout');

    Route::get('/register-doctor', 'AuthController@showRegistrationFormDoctor');
    Route::get('/register', 'AuthController@showRegistrationFormPatient');
    Route::post('/register-doctor', 'PersonController@storeDoctor');
    Route::post('/register', 'PersonController@storePatient');

    Route::get('/search', 'PagesController@showFind');
    Route::post('/find', 'PagesController@showFind');

    Route::get('/profile/{id}/{slug}', 'ProfileController@showProfile');
    Route::get('/settings/{id}/{slug}', 'ProfileController@showSettings');
    Route::post('/profile/{id}/{slug}', 'ProfileController@submitReview');
    Route::get('/reviews/{id}', 'ProfileController@getReviews');

    Route::post('/settings/update', 'UpdateController@updateDoctorInfo');
    Route::post('/settings/update/sched', 'UpdateController@updateDoctorSched');
    Route::post('/settings/update/config', 'UpdateController@updateDoctorConfig');

    Route::get('/sendtest', 'SMSController@goTo');
    Route::post('/sendtest', 'SMSController@sendMessage');



    Route::get('/dashboard', 'DashboardController@showDashboard');
    Route::get('/settings', 'PagesController@showSettings');
    Route::get('/', 'PagesController@showHome');


    Route::post('/queue/add', 'QueueController@addToQueue');
    Route::post('/queue/add2', 'QueueController@addToQueuePortable');
    Route::post('/queue/delete', 'QueueController@removeFromQueue');
    Route::post('/queue/update', 'QueueController@updateQueue');
    Route::get('/queue/get', 'QueueController@getCount');

    Route::post('/schedule/add', 'ScheduleController@addSchedule');
    Route::post('/schedule/delete', 'ScheduleController@deleteSchedule');
    Route::post('/schedule/update', 'ScheduleController@updateSchedule');
    Route::get('/schedule/get', 'ScheduleController@getSchedule');

});

    Route::get('/display','SearchController@displayAll');
    Route::get('/find','SearchController@makeSearch');
    Route::get('/get/settings/{id}',"PersonController@displaySettings");


    Route::post('/add/patient','WalkingController@addNewPatient');
    Route::get('/view/patients','WalkingController@viewAllPatients');
    Route::get('/view/all-patients','WalkingController@viewAll');
