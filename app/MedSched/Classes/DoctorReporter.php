<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 5/13/2016
 * Time: 1:09 PM
 */

namespace App\MedSched\Classes;

use App\MedSched\Interfaces\UserRepositoryInterface;
use App\MedSched\Models\Doctor;
use App\MedSched\Models\DoctorSettings;
use Illuminate\Http\Request;

class DoctorReporter implements UserRepositoryInterface
{

    public function create(Request $request)
    {
        $personCreator = new PersonCreator();
        $person = $personCreator->createPerson($request);

        $doctor = Doctor::create(['specialization' => $request->get('specialization'), 'person_id' => $person->id, 'title' => $person->firstname." ". $person->lastname." ".$person->id]);
        DoctorSettings::create(["doctor_id"=>$doctor->id, 'schedule'=>'8:00,12:00,1:00,5:00|', 'amLimit'=>12, 'pmLimit'=>12]);

    }

    public function delete($id)
    {
        Doctor::findOrFail($id)->delete();
    }

    public function edit(Request $request, $id)
    {
        $doctor = Doctor::findOrFail($id);
        $doctor->fill($request->all())->save();
    }

    public function getDetails($id)
    {
        return Doctor::findOrFail($id)->toArray();
    }
}
