<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 7/6/2016
 * Time: 5:21 AM
 */

namespace App\MedSched\Classes;

use App\MedSched\Interfaces\UserRepositoryInterface;
use App\MedSched\Models\Patient;
use Illuminate\Http\Request;

class PatientReporter implements UserRepositoryInterface
{

    public function create(Request $request)
    {
        $personCreator = new PersonCreator();
        $person = $personCreator->createPerson($request);

        Patient::create(['person_id' => $person->id]);
    }

    public function delete($id)
    {
        Patient::findOrFail($id)->delete();
    }

    public function edit(Request $request, $id)
    {
        $patient = Patient::findOrFail($id);
        $patient->fill($request->all())->save();
    }

    public function getDetails($id)
    {
        return Patient::findOrFail($id)->toArray();
    }
}
