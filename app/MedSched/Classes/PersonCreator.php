<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 5/13/2016
 * Time: 10:47 AM
 */

namespace App\MedSched\Classes;


use App\MedSched\Models\Permission;
use App\MedSched\Models\Person;
use App\MedSched\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PersonCreator
{
    public function createPerson(Request $request){
        $newPerson = Person::create($request->except(['username','password','email_add','contact']));
        $newPerson->birthday = date('F j, Y', strtotime($request->birthday));
        $newPerson->save();
        $this->createUser($request->only(['username','password','email_add','contact']),$newPerson->getAttribute('id'));
        return $newPerson;
    }

    public function createUser($request,$person_id){
        $password = Hash::make($request['password']);
        User::create(['username'=>$request['username'], 'password' => $password, 'email_add' => $request['email_add'], 'contact'=>$request['contact'], 'person_id' => $person_id]);
    }

}
