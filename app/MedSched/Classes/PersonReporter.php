<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 5/13/2016
 * Time: 9:59 AM
 */

namespace App\MedSched\Classes;


use App\MedSched\Interfaces\UserRepositoryInterface;
use App\MedSched\Repositories\PersonRepository;
use Illuminate\Http\Request;

class PersonReporter
{
    /**
     * @var PersonRepository
     */
    private $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function createPerson(Request $request, UserRepositoryInterface $person){
        $this->personRepository->create($request, $person);
    }

//    public function deletePerson(UserRepositoryInterface $person){
//        $this->personRepository->delete($person);
//    }
//
//    public function editPersonDetails($data, UserRepositoryInterface $person){
//        $this->personRepository->edit($data, $person);
//    }
//
//    public function getPersonDetails($person_id){
//
//    }

}