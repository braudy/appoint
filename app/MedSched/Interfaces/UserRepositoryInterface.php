<?php

namespace App\MedSched\Interfaces;


use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function create(Request $request);
    public function delete($id);
    public function edit(Request $request, $id);
    public function getDetails($id);
}