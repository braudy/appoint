<?php

namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Doctor extends Model
{
    use Sluggable;
    protected $table = "doctors";
    protected $fillable = ['specialization','person_id','clinic','rating','title'];

    public function person(){
        return $this->belongsTo('App\MedSched\Models\Person');
    }

    public function queues(){
        return $this->hasMany('App\MedSched\Models\Queue');
    }

    public function reviews(){
        return $this->hasMany('App\MedSched\Models\DoctorReview');
    }

    public function settings(){
        return $this->hasOne('App\MedSched\Models\DoctorSettings');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
