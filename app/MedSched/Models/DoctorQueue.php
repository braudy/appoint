<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 7/9/2016
 * Time: 1:22 AM
 */

namespace App\MedSched\Models;


use Illuminate\Database\Eloquent\Model;

class DoctorQueue extends Model
{   
    protected $table = "doctor_queues";
    protected $fillable = ['slot_number','doctor_id','queue_id'];

    public function queue(){
        return $this->belongsTo('App/MedSched/Models/Queue');
    }
}