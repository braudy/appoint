<?php

namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorReview extends Model
{
    protected $table = 'reviews';
    protected $fillable = ['doctor_id','person_id','review_description','rating'];

    public function doctor(){
        return $this->belongsTo('App\MedSched\Models\Doctor');
    }
}
