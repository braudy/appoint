<?php


namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;

class DoctorSettings extends Model
{
    protected $table = 'settings';
    protected $fillable = ['doctor_id','schedule','amLimit','pmLimit'];

    public function doctor(){
    	return $this->belongsTo('App\MedSched\Models\Doctor');
    }
}
