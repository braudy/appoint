<?php

namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   protected $table = 'notification';
   protected $fillable = ['doctor_id','person_id','message'];
}
