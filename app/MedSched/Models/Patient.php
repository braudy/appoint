<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 7/6/2016
 * Time: 5:27 AM
 */

namespace App\MedSched\Models;


use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $table = "patients";
    protected $fillable = ['person_id'];

    public function person(){
        return $this->belongsTo('App\MedSched\Models\Person');
    }

    public function queues(){
        return $this->hasMany('App\MedSched\Models\Queue');
    }
}