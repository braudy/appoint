<?php

namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = "persons";
    protected $fillable = ['firstname','lastname','address','profile_picture','age','birthday'];

    public function user(){
        return $this->belongsTo('App\MedSched\Models\User','id');
    }

    public function doctor(){
        return $this->hasOne('App\MedSched\Models\Doctor','person_id');
    }

    public function patient(){
        return $this->hasOne('App\MedSched\Models\Patient','person_id');
    }

}
