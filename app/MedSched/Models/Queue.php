<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 7/9/2016
 * Time: 1:21 AM
 */

namespace App\MedSched\Models;


use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    protected $table = "queues";
    protected $fillable = ['doctor_id','patient_id','timeSlot','queueNumber','status'];

    public function doctorQueues(){
        return $this->hasOne('App\MedSched\Models\DoctorQueue');
    }

    public function patientQueues(){
        return $this->hasOne('App\MedSched\Models\PatientQueue');
    }

    public function doctor(){
        return $this->belongsTo('App\MedSched\Models\Doctor');
    }

    public function patient(){
        return $this->belongsTo('App\MedSched\Models\Patient');
    }
}