<?php

namespace App\MedSched\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = "schedules";
    protected $fillable = ['doctor_id','patient_id','timeSlot','date','status'];

    public function doctor(){
        return $this->belongsTo('App\MedSched\Models\Doctor');
    }

    public function patient(){
        return $this->belongsTo('App\MedSched\Models\Patient');
    }
}
