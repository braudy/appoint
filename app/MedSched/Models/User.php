<?php

namespace App\MedSched\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = "users";
    protected $fillable = ['username','password','person_id','contact','email_add'];

    public function person(){
        return $this->hasOne('App\MedSched\Models\Person','id');
    }

    public function doctor(){
        return $this->hasOne('App\MedSched\Models\Doctor','person_id');
    }

    public function patient(){
        return $this->hasOne('App\MedSched\Models\Patient','person_id');
    }
}
