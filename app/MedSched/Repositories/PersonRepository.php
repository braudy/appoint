<?php
/**
 * Created by PhpStorm.
 * User: Braudy
 * Date: 5/13/2016
 * Time: 1:05 PM
 */

namespace App\MedSched\Repositories;


use App\MedSched\Interfaces\UserRepositoryInterface;

class PersonRepository
{
    public function create($request, UserRepositoryInterface $person){
        $person->create($request);
    }
}