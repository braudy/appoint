<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('queues', function ($table) {
            $table->increments('id');
            $table->string('timeSlot');
            $table->string('status');
            $table->integer('queueNumber');
            $table->integer('doctor_id')->references('id')->on('doctors')->onDelete('cascade');;
            $table->integer('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('queues');
    }
}
