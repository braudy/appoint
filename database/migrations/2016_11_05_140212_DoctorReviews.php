<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DoctorReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function ($table) {
            $table->increments('id');
            $table->integer('rating');
            $table->integer('doctor_id')->references('id')->on('doctors')->onDelete('cascade');
            $table->integer('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->string('review_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
