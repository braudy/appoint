<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function ($table) {
            $table->increments('id');
            $table->string('timeSlot');
            $table->string('status');
            $table->string('date');
            $table->integer('doctor_id')->references('id')->on('doctors')->onDelete('cascade');;
            $table->integer('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schedules');
    }
}
