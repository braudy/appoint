<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            'firstname' => 'Braudy',
            'lastname' => 'Pedrosa',
            'address' => 'San Miguel, Iligan City'
        ]);

        DB::table('users')->insert([
            'username' => 'admin',
            'password' => 'admin',
            'email_add' => 'admin@admin.com',
            'contact' => '09361890167',
            'person_id' => '1'
        ]);

        DB::table('patients')->insert([
            'person_id' => '1'
        ]);
    }
}
