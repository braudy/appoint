
var BASE_URL = "http://scheduler.dev/";



$('#search-btn-index').click(function() {
  var keyIndex = $('#search-input-index').val();
});

$('#search-btn').click(function(){
  var key = $('#search-input').val();

  makeSearch(key);
});

function makeSearch(searchQuery){
  var result = '';
  var searchResults = document.getElementsByClassName('search-results');
  $.ajax({
      method: "GET",
      url: BASE_URL + "find/"+ searchQuery,
      dataType: "text",
      success: function( data ) {
        data = JSON.parse(data);

        var result = '<h5>Displaying results for <span>"'+searchQuery+'"</span></h5>'
        '<div class="ap-separator-st2"></div>';

        for( var i=0; i<data.length; i++) {
          var output =  '<div class="post post-search clearfix">'+
                '<img src="../images/'+data[i].profile_picture+'" alt=""/>'+
                '<div class="post-search-content">'+
                    '<div class="col-md-6">'+
                        '<h2>'+data[i].firstname+" "+data[i].lastname+'</h2>'+
                        '<h4>'+data[i].specialization+'</h4>'+

                        '<p>'+data[i].clinic+'</p>'+
                        '<p>'+data[i].address+'</p>'+
                        '<p>'+data[i].contact+'</p>'+
                        '<input value="'+data[i].rating+'" class="review-rating rating-loading">'+
                    '</div>'+
                    '<div class="col-md-6">'+
                        '<ul class="post-ul pull-right post-r-2">'+
                            '<li><a href="'+BASE_URL+"profile/"+data[i].id+"/"+data[i].slug+'">View Profile</a></li>'+
                            '<li><a href="#">Get Appointment</a></li>'+
                        '</ul>'+
                    '</div>'+
                  '</div>'+
                '</div>';
            result += output;
        }
        $(".search-results").html(result);

        result = '<h5>No results for <span>"'+searchQuery+'"</span></h5>'
        '<div class="ap-separator-st2"></div>';

        if(data.length == 0){
          $(".search-results").html(result);
        }

        $('.review-rating').rating({displayOnly:true, step: 0.5});


      }
    });
}
