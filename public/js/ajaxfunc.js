var BASE_URL = "http://scheduler.dev/";



$('#search-btn-index').click(function() {
  var keyIndex = $('#search-input-index').val();
});

$('#search-btn').click(function(){
  var key = $('#search-input').val();
  var specialization = $('#search-specialization').val();

  makeSearch(key,specialization);
});

function makeSearch(searchQuery,specialization){
  var result = '';
  var searchResults = document.getElementsByClassName('search-results');

  var data = {
    _token: $('input[name=_token]').val(),
    searchQuery: searchQuery,
    specialization: specialization,
  };

  $.ajax({
      method: "GET",
      url: BASE_URL + "find/",
      dataType: "text",
      data: data,
      success: function(response) {
        data = JSON.parse(response);

        if(searchQuery == "" && specialization != null){
          var result = '<h5>Displaying results for <span>"'+specialization+'"</span></h5>'
          '<div class="ap-separator-st2"></div>';
        }else if(specialization == null && searchQuery != null){
          var result = '<h5>Displaying results for <span>"'+searchQuery+'"</span></h5>'
          '<div class="ap-separator-st2"></div>';
        }else if(specialization != null && searchQuery != ""){
          var result = '<h5>Displaying results for <span>"'+searchQuery+'"</span> and <span>"'+specialization+'"</span></h5>'
          '<div class="ap-separator-st2"></div>';
        }


        for( var i=0; i<data.length; i++) {
          var output =  '<div class="post post-search clearfix">'+
                '<img src="../images/'+data[i].profile_picture+'" alt=""/>'+
                '<div class="post-search-content">'+
                    '<div class="col-md-6">'+
                        '<h2>'+data[i].firstname+" "+data[i].lastname+'</h2>'+
                        '<h4>'+data[i].specialization+'</h4>'+

                        '<p>'+data[i].clinic+'</p>'+
                        '<p>'+data[i].address+'</p>'+
                        '<p>'+data[i].contact+'</p>'+
                        '<input value="'+data[i].rating+'" class="review-rating rating-loading">'+
                    '</div>'+
                    '<div class="col-md-6">'+
                        '<ul class="post-ul pull-right post-r-2">'+
                            '<li><a href="'+BASE_URL+"profile/"+data[i].id+"/"+data[i].slug+'">View Profile</a></li>'+
                        '</ul>'+
                    '</div>'+
                  '</div>'+
                '</div>';
            result += output;
        }
        $(".search-results").html(result);

        if(searchQuery == "" && specialization != null){
         result = '<h5>No results for <span>"'+specialization+'"</span></h5>'
        '<div class="ap-separator-st2"></div>';
        }else if(specialization == null && searchQuery != null){
          result = '<h5>No results for <span>"'+searchQuery+'"</span></h5>'
        '<div class="ap-separator-st2"></div>';

        }else if(specialization != null && searchQuery != ""){
          var result = '<h5><h5>No results for <span>"'+searchQuery+'"</span> and <span>"'+specialization+'"</span></h5>'
          '<div class="ap-separator-st2"></div>';
        }

     

        if(data.length == 0){
          $(".search-results").html(result);
        }

        $('.review-rating').rating({displayOnly:true, step: 0.5});


      }
    });



 

}
 //scheds
  $('#appointment-modal1-book').click(function(){
    var time = $('#appointment-modal1-time').val();
    var date = $('#appointment-modal1-date').val();

    var data = {
      _token: $('input[name=_token]').val(),
      date: date,
      time: time,
      doctor_id: $('#appointment-modal1-doctor').val(),
      patient_id: $('#appointment-modal1-patient').val(),
    };
    
    $.ajax({
      method: "POST",
      url: BASE_URL + "schedule/add",
      dataType: "text",
      data: data,
      success: function(response) {
     
        swal({
          title: 'Success!',
          text: 'Succesfully booked appointment!',
          type: 'success',
          confirmButtonText: 'Okay'
        });
      }
    
    });
  });

  //resched
  //scheds
  $('#appointment-modal1-resched').click(function(){
    //console.log("working");
    var time = $('#appointment-modal2-time').val();
    var date = $('#appointment-modal2-date').val();

    var data = {
      _token: $('input[name=_token]').val(),
      date: date,
      time: time,
      doctor_id: $('#appointment-modal2-doctor').val(),
      patient_id: $('#appointment-modal2-patient').val(),
      sched_id: $('#appointment-modal2-sched-id').val(),
    };

    console.log(data);
    
    $.ajax({
      method: "POST",
      url: BASE_URL + "schedule/update",
      dataType: "text",
      data: data,
      success: function(response) {
        console.log(response);
        swal({
          title: 'Success!',
          text: 'Succesfully rescheduled appointment!',
          type: 'success',
          confirmButtonText: 'Okay'
        });
    
      }
  });
    });

$('#appointment-modal2-cancel').click(function(){
    var data = {
      _token: $('input[name=_token]').val(),
      sched_id: $(this).attr('class').split(" ")[2],
    };


    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false
    }).then(function () {
       $.ajax({
      method: "POST",
      url: BASE_URL + "schedule/delete",
      dataType: "text",
      data: data,
      success: function(response) {
        console.log(response);
        swal({
          title: 'Success!',
          text: 'Succesfully deleted appointment!',
          type: 'success',
          confirmButtonText: 'Okay'
        }).then(function(){
          location.reload();
        });
    
      }
  });
    }, function (dismiss) {
      // dismiss can be 'cancel', 'overlay',
      // 'close', and 'timer'
      if (dismiss === 'cancel') {
        
      }
    });
    console.log(data);
    
   
    });

  $('.row-hidden').each(function(){
    $(this).click(function(){
      $('#appointment-modal2-patient').val($(this).attr('class').split(' ')[3]);
      $('#appointment-modal2-sched-id').val($(this).attr('class').split(' ')[4]);
    });
  });

  $('#add-patient-button').click(function(){

    var data = {
      _token: $('input[name=_token]').val(),
      age: $('#addPatient_age').val(),
      birthday:$('#addPatient_birthday').val(),
      firstname:$('#addPatient_firstname').val(),
      lastname:$('#addPatient_lastname').val(),
      address:$('#addPatient_address').val(),
      contact:$('#addPatient_contact').val(),
      username:$("#addPatient_firstname").val(),
      password:$('#addPatient_lastname').val(),
      email_add:$("#addPatient_firstname").val() + '@gmail.com',
    }

    console.log(data);

    $.ajax({
      method: "POST",
      url: BASE_URL + "add/patient",
      dataType: "text",
      data: data,
      success: function(response) {
        console.log(response);
        swal({
          title: 'Success!',
          text: 'New Patient Added!',
          type: 'success',
          confirmButtonText: 'Okay'
        });
    
      }
  });
  });


  var canSearch = true;
  var results = "";
  $('#filtery').keyup(function(){
    if ( event.which == 13 ) {
     event.preventDefault();
    }
    canSearch = false;

    var data = {
      _token: $('input[name=_token]').val(),
      search: $(this).val(),
    };

    setTimeout(
    function() 
    {
      canSearch = true;
      
      $.ajax({
        method: "GET",
        url: BASE_URL + "view/patients",
        dataType: "text",
        data: data,
        success: function(response) {
          data = JSON.parse(response);
          results = "";

          for(var i=0; i<data.length;i++){
            var output ='<tr>'+
                   '<td>'+data[i].firstname+' '+data[i].lastname+'</td>'+
                   '<td>'+data[i].birthday+'</td>'+
                   '<td><a href="#" class="btn btn-success table-queue-btn '+data[i].person_id+'">Add to Queue</a></td>'+
                '</tr>';
            results+= output;
          }
          $('#search-queue-results').html(results);

           $('.table-queue-btn').click(function(){
              var id = $(this).attr('class').split(" ")[3];
              saveQueue(id);
            }); 
        }
      });

    }, 500);
  });

  var canSearch2 = true;
  var results2 = "";
  $('#filtert').keyup(function(){
    if ( event.which == 13 ) {
     event.preventDefault();
    }
    canSearch2 = false;

    var data = {
      _token: $('input[name=_token]').val(),
      search: $(this).val(),
    };

    setTimeout(
    function() 
    {
      canSearch2 = true;
      
      $.ajax({
        method: "GET",
        url: BASE_URL + "view/patients",
        dataType: "text",
        data: data,
        success: function(response) {
          data = JSON.parse(response);
          results2 = "";

          for(var i=0; i<data.length;i++){
            var output ='<tr>'+
                   '<td>'+data[i].firstname+' '+data[i].lastname+'</td>'+
                   '<td>'+data[i].birthday+'</td>'+
                   '<td><a href="#" class="btn btn-success table-sched-btn '+data[i].person_id+'">Add to Sched</a></td>'+
                '</tr>';
            results2+= output;
          }
          $('#search-schedule-results').html(results2);

           $('.table-sched-btn').click(function(){
              var id = $(this).attr('class').split(" ")[3];
              $("#appointment-modal1-patient").val(id);

              saveSched(id);
            }); 
        }
      });

    }, 500);
  });


  $('#add-patient-queue-button').click(function(){

    var data = {
      _token: $('input[name=_token]').val(),
      age: $('#addQueuePatient_age').val(),
      birthday:$('#addQueuePatient_birthday').val(),
      firstname:$('#addQueuePatient_firstname').val(),
      lastname:$('#addQueuePatient_lastname').val(),
      address:$('#addQueuePatient_address').val(),
      contact:$('#addQueuePatient_contact').val(),
      username:$("#addQueuePatient_firstname").val(),
      password:$('#addQueuePatient_lastname').val(),
      email_add:$("#addQueuePatient_firstname").val() + '@gmail.com',
    }

    console.log(data);

    $.ajax({
      method: "POST",
      url: BASE_URL + "add/patient",
      dataType: "text",
      data: data,
      success: function(response) {
        var id = response;
        saveQueue(id);
      }
  });
  });

  $('#add-patient-schedule-button').click(function(){

    var data = {
      _token: $('input[name=_token]').val(),
      age: $('#addSchedulePatient_age').val(),
      birthday:$('#addSchedulePatient_birthday').val(),
      firstname:$('#addSchedulePatient_firstname').val(),
      lastname:$('#addSchedulePatient_lastname').val(),
      address:$('#addSchedulePatient_address').val(),
      contact:$('#addSchedulePatient_contact').val(),
      username:$("#addSchedulePatient_firstname").val(),
      password:$('#addSchedulePatient_lastname').val(),
      email_add:$("#addSchedulePatient_firstname").val() + '@gmail.com',
    }

    console.log(data);

    $.ajax({
      method: "POST",
      url: BASE_URL + "add/patient",
      dataType: "text",
      data: data,
      success: function(response) {
        var id = response;
        saveSched(id);
      }
  });
  });

  function saveQueue(id){
    var data = {
      _token: $('input[name=_token]').val(),
      doctor_id: $('#queueModalDoctorID').val(),
      patient_id: id,
      timeSlot: $('#queueModalTimeSlot').val(),
      queueNumber: $('#queueModalQueueNumber').val(),
    }

    $.ajax({
      method: "POST",
      url: BASE_URL + "queue/add2",
      dataType: "text",
      data: data,
      success: function(response) {
        $('#addQueuePatient_age').val("");
        $('#addQueuePatient_contact').val("");
        $('#addQueuePatient_address').val("");
        $('#addQueuePatient_lastname').val("");
        $('#addQueuePatient_firstname').val("");
        $('#addQueuePatient_birthday').val("");

      
        swal({
          title: 'Success!',
          text: 'Successfully added to queue!',
          type: 'success',
          confirmButtonText: 'Okay'
        });
    
      }
  });
  }

  

  function saveSched(id){
    var time = $('#appointment-modal1-time').val();
    var date = $('#appointment-modal1-date').val();

    var data = {
      _token: $('input[name=_token]').val(),
      date: date,
      time: time,
      doctor_id: $('#appointment-modal1-doctor').val(),
      patient_id: id,
    };

    $.ajax({
      method: "POST",
      url: BASE_URL + "schedule/add",
      dataType: "text",
      data: data,
      success: function(response) {
     
        swal({
          title: 'Success!',
          text: 'Succesfully booked an appointment!',
          type: 'success',
          confirmButtonText: 'Okay'
        });
      }
    
    });
  }

  

 
