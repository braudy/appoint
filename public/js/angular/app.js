(function() {
  var app = angular.module('appoint', ['registration'], function($interpolateProvider) {
      $interpolateProvider.startSymbol('<%');
      $interpolateProvider.endSymbol('%>');
  });
})();
