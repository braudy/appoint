(function() {
  angular.module('registration', [])

  .controller('RegistrationController', function($http,$scope){
    $scope.registration = {};
    $scope.religions = religion;
    $scope.specialzations = specialzation;
    $scope.prompt = "";
    $scope.tab;


    $scope.registerUser = function() {
      $scope.registration.role_id = $scope.tab;
      $http({
        method:'POST',
        url: "/",
        data: JSON.stringify($scope.registration)
      }).then(function success(response){
        //handle success
        $scope.prompt = "Successfully registered!";
        console.log(response);
      });
    };

  });

  var religion = ['Roman Catholic', 'Protestant', 'Catholic'];
  var specialzation = ['Dentistry/Oral Surgery','Dermatology','Gynecology','Maternal and Fetal Medicine','Neurology','Ophthalmology','Pediatrics'];
})();
