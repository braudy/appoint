$(document).ready(function() {
    $('.footable').footable({
        breakpoints: {
            phone: 480,
            tablet: 991
        }
    });


    $('table').trigger('footable_expand_first_row');
    $('table').bind('footable_breakpoint', function() {
        $('table').trigger('footable_expand_first_row');
    });

    //filter
    $('.filter-status').change(function(e) {
        e.preventDefault();
        $('table').trigger('footable_filter', {
            filter: $('.filter-status').val()
        });
    });

    $('.clear-filter').click(function(e) {
        e.preventDefault();
        $('#filter').text('');
        $('.filter-status').val('');
        $('table').trigger('footable_clear_filter');
    });
    ///
    var toggled = false;
    $('.header-toggler button').click(function() {
        $('.ap-dboard-header').slideToggle("slow");
        if (!toggled)
            toggled = true;
        else
            toggled = false;
    });

    var target;
    var somethingIsOpen = false;

    $('.ap-dboard-menu').each(function() {
        $(this).click(function(e) {
            target = $(this).data("target");
            if (target != "#notifs") {
                $('#account-menu').slideToggle();
                $('#notifs').hide();
            } else {
                $('#notifs').slideToggle();
                $('#account-menu').hide();
            }
        });
    });




    var isTab = false;
    setInterval(function() {
        var viewportWidth = $(window).width();
        if (viewportWidth < 991) {
            isTab = true;
            $('.ap-dboard-ul li i').addClass('fa-2x');
            if (isTab) {
                $('.header-toggler').show();
                if (toggled) {
                    if ($(window).scrollTop() == 0) {
                        //$('.ap-dboard-header').slideDown("fastest");
                    } else {
                        toggled = false;
                        $('.ap-dboard-header').slideUp("slow");
                        $(target).hide();
                        toggled = true;
                    }
                }
            }
        } else {
            isTab = false;
            $('.header-toggler').hide();
            $('.ap-dboard-header').show();
            $('.ap-dboard-ul li i').removeClass('fa-2x');
        }
    }, 50);

    //adjust height of flex rows
    if ($(window).width() > 992)
        $('.row-flex').css("min-height", $(window).height());

    $.smoothScroll();

    //custom select
    if ($('.ap-input-select option').filter(':selected').val() == $('.ap-input-select').find('.placeholder').val())
        $('.ap-input-select').css('color', 'grey');

    $('.ap-input-select').each(function() {
        $(this).click(function() {
            if ($(this).find(':selected').val() != $(this).find('.placeholder').val())
                $(this).css('color', 'black');
        });
    });


    //rating
    $('#static-rating').rating({
        displayOnly: true,
        step: 0.5
    });


    $('.queue-btns').click(function() {
        $('#queue-modal').modal("toggle");
    });


    $('.queue-btns-doc').click(function() {
        $('#openSearchModal').modal("toggle");
        // $('#addToQueueModal').modal("toggle");
        // console.log('working');
    });




    // $("#ap_carousel").owlCarousel({
    //
    // navigation : true, // Show next and prev buttons
    // slideSpeed : 300,
    // paginationSpeed : 400,
    // singleItem:true,
    // autoPlay: 4000,
    // });

    // Call this from the developer console and you can control both instances
    var calendars = {};

    var thisMonth = moment().format('YYYY-MM');
    // Events to load into calendar
    var eventArray = [];

    calendars.clndr1 = $('.cal1').clndr({
        events: eventArray,
        clickEvents: {
            click: function(target) {
                //console.log('Cal-1 clicked: ', target);
            },
            today: function() {
                console.log('Cal-1 today');
            },
            nextMonth: function() {
                console.log('Cal-1 next month');
            },
            previousMonth: function() {
                console.log('Cal-1 previous month');
            },
            onMonthChange: function() {
                removeBtns();
            },
            nextYear: function() {
                console.log('Cal-1 next year');
            },
            previousYear: function() {
                console.log('Cal-1 previous year');
            },
            onYearChange: function() {
                console.log('Cal-1 year changed');
            },
            nextInterval: function() {
                console.log('Cal-1 next interval');
            },
            previousInterval: function() {
                console.log('Cal-1 previous interval');
            },
            onIntervalChange: function() {
                console.log('Cal-1 interval changed');
            }
        },
        multiDayEvents: {
            singleDay: 'date',
            endDate: 'endDate',
            startDate: 'startDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false,
        daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        ignoreInactiveDaysInSelection: true
    });

    // Bind all clndrs to the left and right arrow keys
    $(document).keydown(function(e) {
        // Left arrow
        if (e.keyCode == 37) {
            calendars.clndr1.back();
            calendars.clndr2.back();
            calendars.clndr3.back();
        }

        // Right arrow
        if (e.keyCode == 39) {
            calendars.clndr1.forward();
            calendars.clndr2.forward();
            calendars.clndr3.forward();
        }
    });
});

//end of document ready

function removeBtns() {

    var id = $('#hiddenID').val();
    var oldArray = ["0", "1", "2", "3", "4", "5", "6"];

    $.ajax({
        method: "GET",
        url: BASE_URL + "get/settings/" + id,
        dataType: "text",
        data: id,
        success: function(response) {
            data = JSON.parse(response);

            var split = (data.schedule).split("|");
            var nums = split[1].split(",");

            var diff = $(oldArray).not(nums).get();

            $('.day').each(function() {
                for (var o = 0; o < diff.length; o++) {
                    if ($(this).hasClass('calendar-dow-' + diff[o])) {
                        $(this).addClass('inactive');
                        $(this).find('#day-icon').css('display', 'none');
                    }
                }
            });
        }
    });

    //disable 2 days ahead
    for (var i = 0; i < 3; i++) {
        var date = moment();
        var day = date.add(i, 'days').format("YYYY-MM-DD");
        var cut = ".calendar-day-" + day;
        if (i == 0) {
            $(cut).removeClass('today');
            $(cut).addClass('inactive-today');
        } else
            $(cut).addClass('inactive');
    }
    //remove buttons for inactive dates
    $('.day').each(function() {
        if ($(this).hasClass('past') || $(this).hasClass('inactive-today') || $(this).hasClass('inactive') || $(this).hasClass('adjacent-month')) {
            $(this).find('#day-icon').css('display', 'none');
        } else if (!$(this).hasClass('inactive')) {

            $(this).css('cursor', 'pointer');
            $(this).click(function() {
              var date = ($(this).attr('class').split(" ")[1]).substring(13);
              $('#appointment-modal1-date').val(($(this).attr('class').split(" ")[1]).substring(13));
              $('#appointment-modal2-date').val(($(this).attr('class').split(" ")[1]).substring(13));

              if($("#secretLoveSong").val() == "true"){
                $('#openSearchSchedModal').modal('toggle');
                getSched(date);
              }
              else{
                $('#appointment-modal1').modal('toggle');
                getSched(date);
              } 
                
            });

        }
    });

    //dataTable
}
var results3 = "";
function getSched(date){
  var data ={
     _token: $('input[name=_token]').val(),
      date: date,
  };

  $.ajax({
      method: "GET",
      url: BASE_URL + "schedule/get",
      dataType: "text",
      data: data,
      success: function(response) {
        data = JSON.parse(response);
        console.log(data);

        results3 = "";

          for(var i=0; i<data.length;i++){
            var output = '<li style="font-size: 13px;">'+data[i].firstname+' '+data[i].lastname+'</li>';
            results3+= output;
          }
          $('#calendar_patients_list').html(results3);

      }
    
    });
}