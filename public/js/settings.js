

var BASE_URL = "http://scheduler.dev/";

var days = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday","weekdays","all"];

$('#saveProfileChanges').click(function(){

  var firstname = $("#set_fname").val();
  var lastname = $("#set_lname").val();
  var specialization = $("#set_specialization").val();
  var contact = $("#set_contact").val();
  var caddress = $("#set_caddress").val();
  var haddress = $("#set_haddress").val();
  var email = $("#set_email").val();
  var description = $("#set_description").val();

  $('#infoForm').submit();
});


$('#saveScheduleChanges').click(function(){


  var selected = $('input[type="checkbox"]:checked');

  var lastLink = window.location.href;
  var selectedIDs = "";
  var sched = "";

  for(var i=0;i < 4;i++){
    if(i < 3){
      sched += $('#time_'+i).val()+",";
    }else{
       sched += $('#time_'+i).val();
    }
  }

  for (var i = 0; i < selected.length; i++) {
    if(i < selected.length-1){
      selectedIDs += selected[i].value+",";
    }else{
      selectedIDs += selected[i].value;
    }
  }

  storeSchedule(sched+"|"+selectedIDs+"", lastLink);

});

  
function storeSchedule(sched, URL){
  var data = {
    _token: $("#tokenF").val(),
    sched: sched,
    URL: URL,
  };

  $.ajax({
      method: "POST",
      url: BASE_URL + "settings/update/sched",
      data: data,
       success:function(data){
          console.log(data);
      }
    });

    swal({
      title: 'Success!',
      text: 'Successfully saved changes!',
      type: 'success',
      confirmButtonText: 'Okay'
    });
}

$('#saveConfig').click(function(){
  var am = $("#amLimit").val();
  var pm = $("#pmLimit").val();

  storeConfig(am,pm);
});

  
function storeConfig(am, pm){
  var data = {
    _token: $("#tokenF").val(),
    am: am,
    pm: pm,
  };

  $.ajax({
      method: "POST",
      url: BASE_URL + "settings/update/config",
      data: data,
       success:function(data){
          console.log(data);
      }
    });

  swal({
      title: 'Success!',
      text: 'Successfully saved changes!',
      type: 'success',
      confirmButtonText: 'Okay'
    });
}


