<!DOCTYPE html>
<html ng-app="appoint">
<meta name="_token" content="{!! csrf_token() !!}"/>
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="{{ URL::asset('css/clndr.css')}}">
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/slider.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('star-rating/css/star-rating.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('font-awesome-4.5.0/css/font-awesome.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/style.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/media_queries.css') }}"></link>
  <link rel="stylesheet" href="{{ URL::asset('footable/css/footable.core.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('footable/css/footable.metro.css') }}">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.css">
  <link rel="stylesheet" href="{{ URL::asset('css/modif.css') }}">

	@yield('title')
</head>
@if(Auth::check())
  @if(Auth::user()->person->doctor != null)
  <input type="hidden" id="hiddenID" value="{{Auth::user()->id}}">
  @endif
@endif
<body onload="removeBtns()">
  @yield('content')

   <script type="text/javascript" src="{{ URL::asset('js/jquery-2.2.0.min.js')}}"></script>
   <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{ URL::asset('js/smoothscroll.js')}}"></script>
   <script type="text/javascript" src="{{ URL::asset('star-rating/js/star-rating.min.js')}}"></script>
   <!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.min.js"></script>
   <script type="text/javascript" src="js/angular/app.js"></script>
   <script type="text/javascript" src="js/angular/registration.js"></script> -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
   <script type="text/javascript" src="{{ URL::asset('footable/js/footable.js')}}"></script>
   <script src="{{ URL::asset('footable/js/footable.sort.js')}}" type="text/javascript"></script>
   <script src="{{ URL::asset('footable/js/footable.filter.js')}}" type="text/javascript"></script>
   <script src="{{ URL::asset('footable/js/footable.paginate.js')}}" type="text/javascript"></script>
   <script type="text/javascript" src="{{ URL::asset('js/clndr.js')}}"></script>
   <script type="text/javascript" src="{{ URL::asset('js/custom.js')}}"></script>
   <script type="text/javascript" src="{{ URL::asset('js/ajaxfunc.js')}}"></script>
  <script type="text/javascript" src="{{ URL::asset('js/settings.js')}}"></script>
  <script src="https://cdn.jsdelivr.net/sweetalert2/6.1.1/sweetalert2.min.js"></script>

   <script type="text/javascript">
    $(document).ready(function() {
     $('.review-rating').rating({displayOnly:true, step: 0.5});
   });
   </script>
</body>
<script type="text/javascript">
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>
</html>
