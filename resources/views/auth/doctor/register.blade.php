@extends('app')

@section('title')
    <title>Register Doctor</title>
@stop

@section('content')
    <div class="container text-center">
        <div id="login-area">
            <div id="login-welcome">
                <h5>MedSched</h5>
            </div>
            <form action="{{url('/register-doctor')}}" method="post">
                @if (Session::has('flash_notification.message'))
                    <h3 class="flash-{{ Session::get('flash_notification.level')}}">{{ Session::get('flash_notification.message') }}</h3>
                @endif
                {!! csrf_field() !!}
                <input type="text" class="ap-input text-left" name="username" value="" placeholder="Username" required>

                <input type="password" class="ap-input text-left" name="password" value="" placeholder="Password" required>

                <input type="text" class="ap-input text-left" name="firstname" value="" placeholder="First Name" required>

                <input type="text" class="ap-input text-left" name="lastname" value="" placeholder="Last Name" required>

                <input type="text" class="ap-input text-left" name="address" value="" placeholder="Address" required>

                <input type="text" class="ap-input text-left" name="contact" value="" placeholder="Contact Number" required>

                <input type="text" class="ap-input text-left" name="email_add" value="" placeholder="Email Address" required>

                <select class="ap-input ap-input-select" name="specialization">
                   <option class="placeholder" disabled selected hidden>Specialization</option>

                   <option value="Family Medicine">Family medicine</option>
                   <option value="Internal medicine">Internal medicine</option>
                   <option value="Psychiatry">Psychiatry</option>
                   <option value="Emergency Medicine">Emergency medicine</option>
                   <option value="Pediatrics">Pediatrics</option>
                   <option value="Obstetrics and Gynecology">Obstetrics and gynecology</option>
                   <option value="General Surgery">General surgery</option>
                   <option value="Neurology">Neurology</option>
                   <option value="Orthopedic Surgery">Orthopedic surgery</option>
                   <option value="Physician Assistant">Physician assistant</option>
                   <option value="Hematology Oncology">Hematology and/or oncology</option>
                   <option value="Otolaryngology">Otolaryngology</option>
                   <option value="Cardiology">Cardiology</option>
                   <option value="Gastroenterology">Gastroenterology</option>
                   <option value="Urology">Urology</option>
                   <option value="Pulmonolgy ">Pulmonolgy </option>
                   <option value="Dermatology">Dermatology</option>
                   <option value="Geriatrics">Geriatrics</option>
                 </select>
                <button type="submit" class="ap-btn ap-btn-primary">Register</button>
            </form>
            <div class="ap-separator-st2"></div>
            <p>
                Already have an account?
            </p>
            <a href="/login" class="ap-btn ap-btn-primary">Login</a>


        </div>

    </div>

@stop
