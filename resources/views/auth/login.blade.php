@extends('app')

@section('title')
    <title>Login</title>
@stop

@section('content')
    <div class="container text-center">
        <div id="login-area">
            <div id="login-welcome">
                <a href="/" class="no-decoration"><h5>MedSched</h5></a>
            </div>
            <form action="{{url('/login')}}" method="post">
            @if (Session::has('flash_notification.message'))
                <h3 class="flash-{{ Session::get('flash_notification.level')}}">{{ Session::get('flash_notification.message') }}</h3>
            @endif
            {!! csrf_field() !!}
            <input type="text" class="ap-input text-left" name="username" value="" placeholder="Username">
            <input type="password" class="ap-input text-left" name="password" value="" placeholder="Password">
                <button type="submit" class="ap-btn ap-btn-primary">Login</button>
            </form>
            <div class="ap-separator-st2">  </div>
            <p>
                Do not have an account?
            </p>
            <a href="/register" class="ap-btn ap-btn-primary">Create an Account</a>


        </div>

    </div>

@stop