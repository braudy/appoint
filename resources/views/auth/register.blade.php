@extends('app')

@section('title')
    <title>Register</title>
@stop

@section('content')
    <div class="container text-center">
        <div id="login-area">
            <div id="login-welcome">
                <h5>MedSched</h5>
            </div>
            <form action="{{url('/register')}}" method="post">
                @if (Session::has('flash_notification.message'))
                    <h3 class="flash-{{ Session::get('flash_notification.level')}}">{{ Session::get('flash_notification.message') }}</h3>
                @endif
                {!! csrf_field() !!}

                <input type="text" class="ap-input text-left" name="username" value="" placeholder="Username" required>

                <input type="password" class="ap-input text-left" name="password" value="" placeholder="Password" required>

                <input type="text" class="ap-input text-left" name="firstname" value="" placeholder="First Name" required>

                <input type="text" class="ap-input text-left" name="lastname" value="" placeholder="Last Name" required>

                <input type="number" class="ap-input text-left" name="age" value="" placeholder="Age" required>

                <input type="text" class="ap-input text-left" name="address" value="" placeholder="Address" required>

                <input type="number" class="ap-input text-left" name="contact" value="" placeholder="Contact Number" required>

                <input type="email" class="ap-input text-left" name="email_add" value="" placeholder="Email Address" required>
                <button type="submit" class="ap-btn ap-btn-primary">Register</button>
            </form>
            <div class="ap-separator-st2"></div>
            <p>
                Already have an account?
            </p>
            <a href="/login" class="ap-btn ap-btn-primary">Login</a>


        </div>

    </div>

@stop
