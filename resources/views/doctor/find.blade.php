@extends('app')

@section('title')
    <title>Find Doctor</title>
@stop

@section('content')
    @include('header')

    <div class="ap-container container rmv-margin">
        <div id="box_150" id="search-results">
            <div id="box_180">
                <h2>Search</h2>
            </div>

            <div class="container" id="search-area">
                <div class="col-md-18 rmv-left-padding">
                    <div id="box_180">

                        <h5>Search Filters</h5>
                        <div class="ap-icon-input">
                            <i class="fa fa-search"></i>
                            <input type="text" class="ap-input" id="search-input" placeholder="Search keyword .. "></input>
                            {{csrf_field()}}
                        </div>
                        <div class="ap-icon-input">
                            <i class="fa fa-user-md"></i>
                            <select class="ap-input ap-input-select" name="specialization" id="search-specialization">
                              <option class="placeholder" disabled selected hidden>Specialization</option>
                              <option value="family-medicine">Family medicine</option>
                              <option value="General Physician">General Physician</option>
                              <option value="psychiatry">Psychiatry</option>
                              <option value="emergency-medicine">Emergency medicine</option>
                              <option value="pediatrics">Pediatrics</option>
                              <option value="obstetrics-and-gynecology">Obstetrics and gynecology</option>
                              <option value="general-surgery">General surgery</option>
                              <option value="neurology">Neurology</option>
                              <option value="orthopedic surgery">Orthopedic surgery</option>
                              <option value="physician assistant">Physician assistant</option>
                              <option value="hematology-oncology">Hematology and/or oncology</option>
                              <option value="otolaryngology">Otolaryngology</option>
                              <option value="cardiology">Cardiology</option>
                              <option value="gastroenterology">Gastroenterology</option>
                              <option value="urology">Urology</option>
                              <option value="pulmonolgy ">Pulmonolgy </option>
                              <option value="dermatology">Dermatology</option>
                              <option value="geriatrics">Geriatrics</option>


                            </select>
                        </div>
                        <button id="search-btn" class="ap-btn ap-btn-primary">Search</button>
                    </div>

                </div>

                <div class="col-md-20 rmv-right-padding">
                    <div id="box_180" class="search-results">
                        <h5>Search results will be displayed here ..</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
