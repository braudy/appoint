@extends('app')

@section('title')
  <title>Profile</title>
  <input type="hidden" id="hiddenID" value="{{$data->person_id}}">
@stop

@section('content')
   @include('header')
   <div class="ap-container container">
     <div id="box_150">
       <div class="row row-flex">

         <div class="col-md-13 col-flex">
           <div id="box_180">
             <div id="profile">
               <div id="profile-img" class="text-center">
                 <img src="http://scheduler.dev/images/{{$data->profile_picture}}" alt="" />
               </div>

               <div id="specialization">{{$data->specialization}}</div>
               <h5 id="name">{{$data->firstname ." ". $data->lastname}}</h5>
               @if($data->clinic != null)
               <p id="workplace">Working at {{$data->clinic}}</p>
               <p id="member">Since {{$data->clinic_date}}</p>
               @endif
              <div class="ap-separator-st2"></div>
               <h5 id="title">About</h5>
               <p id="about">{{$data->description}}</p>
               <div class="ap-separator-st2"></div>
               <h5 id="title">Rating</h5>
               <!-- <img class="img-responsive" src="images/stars.png" alt="" /> -->
               <input value="{{$data->rating}}" class="review-rating rating-loading">

             </div>
           </div>
         </div>



         <div class="col-md-16 col-flex">
           <div id="box_180">
             <div id="content">
               <ul class="ap-tabs">
                 <li><a data-toggle="tab" href="#tab1">Queuer</a></li>
                 <li><a data-toggle="tab" href="#tab4">Scheduler</a></li>
                 <!-- <li><a data-toggle="tab" href="#tab2">About</a></li> -->
                 <li><a data-toggle="tab" href="#tab3">Reviews</a></li>
                 <div class="ap-separator-st2"></div>
               </ul>
               <!-- <div id="profile-post">
                 <i class="fa fa-comments-o fa-2x"></i>
                 <h5>Patient Reviews</h5>

               </div> -->


               <div id="content-details">
                 <div class="tab-content">
                  <div id="tab1" class="tab-pane fade in active">
                    <div class="ap-container">
                      <ul class="ap-ul ap-ul-2c">
                      @if(date("A") == "PM")
                        <li>Slot Limit: {{$settings->amLimit}}</li>
                      @else
                        <li>Slot Limit: {{$settings->pmLimit}}</li>
                      @endif
                      </ul>
                      @if(Auth::check())
                        @if(Auth::user()->person->doctor != null)
                        <div id="booking-btns">
                        <div class="row">
                          <div style="cursor: pointer;" class="queue-btns-doc col-md-6">
                            <div id="box_100">
                              <h1>AM</h1>
                              <h2>Office Hours</h2>
                              <h3>{{$time[2]}} - {{$time[3]}}</h3>
                              <h4>{{$settings->pmLimit}} slots available</h4>
                            </div>
                          </div>
                          <div style="cursor: pointer;" class="queue-btns-doc col-md-6">
                            <div id="box_100">
                              <h1>PM</h1>
                              <h2>Office Hours</h2>
                              <h3>{{$time[0]}} - {{$time[1]}}</h3>
                              <h4>{{$settings->amLimit  - $amQueue}} slots available</h4>
                            </div>
                          </div>
                        </div>
                      </div>
                        @else
                        <div id="booking-btns">
                        <div class="row">
                          <div class="queue-btns col-md-6">
                            <div id="box_100">
                              <h1>AM</h1>
                              <h2>Office Hours</h2>
                              <h3>{{$time[2]}} - {{$time[3]}}</h3>
                              <h4>{{$settings->pmLimit}} slots available</h4>
                            </div>
                          </div>
                          <div class="queue-btns col-md-6">
                            <div id="box_100">
                              <h1>PM</h1>
                              <h2>Office Hours</h2>
                              <h3>{{$time[0]}} - {{$time[1]}}</h3>
                              <h4>{{$settings->amLimit  - $amQueue}} slots available</h4>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                        @endif
                      @endif
                      

                      <div id="booked">
                          @for ($i = 1; $i <= sizeOf($queues); $i++)
                          @if($i == 1)
                          <div class="post-serving">
                            <div class="col-md-17">
                              <div class="currently-serving-sm serving">
                                {{$queues[$i-1]["queueNumber"]}}
                              </div>
                            </div>
                            <div class="col-md-19">
                              <div class="post-serving-content">
                                Now serving ..
                                <span class="pull-right time">Time Slot: {{$queues[$i-1]["timeSlot"]}} </span>
                              </div>
                            </div>
                          </div>
                          @elseif($i == 2)
                          <div class="post-serving">
                            <div class="col-md-17">
                              <div class="currently-serving-sm">
                                {{$queues[$i-1]["queueNumber"]}}
                              </div>
                            </div>
                            <div class="col-md-19">
                              <div class="post-serving-content">
                                Next ..
                                <span class="pull-right time">Time Slot: {{$queues[$i-1]["timeSlot"]}} </span>
                              </div>
                            </div>
                          </div>
                          @else
                          <div class="post-serving">
                            <div class="col-md-17">
                              <div class="currently-serving-sm">
                                {{$queues[$i-1]["queueNumber"]}}
                              </div>
                            </div>
                            <div class="col-md-19">
                              <div class="post-serving-content">
                                Will be served in {{$estimates[$i-3]}}
                                <span class="pull-right time">Time Slot: {{$queues[$i-1]["timeSlot"]}} </span>
                              </div>
                            </div>
                          </div>
                          @endif

                          @if($i != sizeOf($queues))
                          <div class="ap-separator-st2"></div>
                          @endif
                          @endfor
                      </div>
                    </div>

                    </div>

              

                <div id="tab3" class="tab-pane fade">
                    <div class="review row">
                      <div class="col-md-17">
                        <div class="review-img">
                          {{substr(Auth::user()->person()->first()->firstname, 0, 1)}}
                        </div>
                      </div>
                      <form action="{{url()->full()}}" method="post">
                      <div class="col-md-19 review-content">
                        <h3>{{Auth::user()->person()->first()->firstname}} {{Auth::user()->person()->first()->lastname}}</h3>
                           {!! csrf_field() !!}
                          <input id="input-1" name="rating" class="rating rating-loading" data-min="0" data-max="5" data-step="1">
                          <input type="hidden" name="doctor_id" value="{{$data->id}}">
                          <textarea name="review" class="ap-input" rows="5" placeholder="Write a review ..."></textarea>
                          <button type="submit" class="ap-btn ap-btn-primary"> Submit</a>
                        </form>
                      </div>
                    </div>
                    <div class="ap-separator"></div>
                       @foreach($reviews as $review)
                    <div class="review-rated row">
                      <div class="col-md-17">
                        <div class="review-img">
                         
                            {{substr($review['firstname'],0,1)}}
                        
                        </div>
                      </div>
                      <div class="col-md-19 review-content">
                        <h3>{{$review['firstname']}} {{$review['lastname']}}</h3>
                        <input value="{{$review['rating']}}" class="review-rating rating-loading">
                        <p>
                          {{$review['review_description']}}
                        </p>
                      </div>
                    </div>
                    <div class="ap-separator-st2"></div>
                      @endforeach
                </div>

                <div id="tab4" class="tab-pane fade">
                  <div id="set-appointment">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="cal1"></div>
                      </div>
                    </div>
                  </div>
                </div>



                 </div>
               </div>
               <!-- queue Modal -->
               <div id="queue-modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                        </div>
                        <form action="{{url('queue/add')}}" method="post">
                          {!! csrf_field() !!}
                          <input type="hidden" id="queueModalDoctorID" value="{{$data->person_id}}" name="doctor_id">
                          @if(sizeOf($queues_all) == 0)
                          <input type="hidden" id="queueModalTimeSlot" value="{{$firstTimeSlot}}" name="timeSlot">
                          @else
                          <input type="hidden" id="queueModalTimeSlot" value="{{$startPrevTime}} - {{$nextTime}}" name="timeSlot">
                          @endif
                          <input type="hidden" id="queueModalQueueNumber" value="{{sizeOf($queues_all)+1}}" name="queueNumber">
                           <div class="modal-body">
                              <div class="row">
                                 <div id="queue-content" class="col-md-12">
                                    <h5>You will be</h5>
                                    <h4>#{{sizeOf($queues_all)+1}}</h4>
                                    @if(sizeOf($queues) == 0)
                                      <h5>You can be served immediately</h5>
                                    @else
                                      <h5>Estimated time to be served is</h5>
                                    <h4>{{(sizeOf($queues)+1)*$amTimeInterval}} minutes</h4>
                                    @endif
                                    
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <button type="submit" class="ap-btn ap-btn-primary ap-btn-half">Get number</button>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- queue Modal end -->

               <!-- appointment Modal -->
               <div id="appointment-modal1" class="modal fade" role="dialog">
                  <div class="modal-dialog modal-dialog-custom">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Set an appoinment</h4>
                        </div>
                        <form action="" method="post">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-12">
                                 @if(Auth::check())
                                  @if(Auth::user()->person->doctor != null)
                                    <label for="username">Time</label>
                                    <input type="hidden" id="appointment-modal1-date">
                                    <input type="hidden" value="{{$data->person_id}}" id="appointment-modal1-doctor">
                                    <input type="hidden" value="" id="appointment-modal1-patient">
                                  @else
                                    <div class="col-md-8">
                                      <label for="username">Time</label>
                                    <input type="hidden" id="appointment-modal1-date">
                                    <input type="hidden" value="{{$data->person_id}}" id="appointment-modal1-doctor">
                                    <input type="hidden" value="{{Auth::user()->person_id}}" id="appointment-modal1-patient">
                                    <select class="ap-input ap-input-select" id="appointment-modal1-time">
                                       @for($i = 0 ; $i < sizeOf($timeSlots); $i++)
                                          <option value="{{$timeSlots[$i]}}">{{$timeSlots[$i]}}</option>
                                       @endfor
                                    </select>
                                    </div>
                                    <div class="col-md-4">
                                      <div id="box_150">
                                        <h3>Booked Patients</h3>

                                        <ul id="calendar_patients_list" style="list-style: none;padding-left: 0px!important;">
                                          
                                        </ul>
                                      </div>
                                    </div>
                                  @endif
                                @endif  
                                    
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <a href="#" id="appointment-modal1-book" class="ap-btn ap-btn-primary ap-btn-half">Book</a>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
               <!-- appointment Modal end -->



            <!-- search patients Modal -->
               <div id="openSearchModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Search Patient</h4>
                           <i data-toggle="modal" data-target="#addToQueueModal" class="fa fa-plus fa-lg ap-dboard" aria-hidden="true" style="color:green; cursor: pointer;"><p style="display:inline; margin-left: 5px;">Add</p></i>
                        </div>

                        <div class="ap-container container">
                        <p>
                          Search: <input id="filtery" class="ap-input" type="text">
                          <a href="#clear" class="clear-filter" title="clear filter">[clear]</a>
                        </p>
                        <table class="table table-striped footable toggle-square" data-page-navigation=".pagination"  data-filter-text-only="true" data-filter="#filtery">
                         <thead>
                        
                           <tr>
                             <th data-hide="phone">Name</th>
                             <th>Birthdate</th>
                             <th>Action</th>
                           </tr>
                           
                         </thead>
                           <tbody id="search-queue-results">
                          
                      
                            
                           
                             
                           </tbody>
                           <tfoot class="hide-if-no-paging text-center">
                              <tr>
                                <td colspan="6">
                                  <div class="pagination"></div>
                                </td>
                              </tr>
                            </tfoot>

                          </table>
                      </div>
                        
                     </div>
                  </div>
               </div>
            <!-- search patients Modal -->

            <!-- search patients Modal -->
               <div id="openSearchSchedModal" class="modal fade" role="dialog">
                  <div class="modal-dialog modal-dialog-custom">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Search Patient</h4>
                           <i data-toggle="modal" data-target="#addToScheduleModal" class="fa fa-plus fa-lg ap-dboard" aria-hidden="true" style="color:green; cursor: pointer;"><p style="display:inline; margin-left: 5px;">Add</p></i>
                        </div>

                        <div class="ap-container container">
                        <div class="col-md-8">
                          <p>
                          Search: <input id="filtert" class="ap-input" type="text">
                          <a href="#clear" class="clear-filter" title="clear filter">[clear]</a>
                        </p>
                        <table class="table table-striped footable toggle-square" data-page-navigation=".pagination"  data-filter-text-only="true" data-filter="#filtert">
                         <thead>
                        
                           <tr>
                             <th data-hide="phone">Name</th>
                             <th>Birthdate</th>
                             <th>Action</th>
                           </tr>
                           
                         </thead>
                           <tbody id="search-schedule-results">
                          
                      
                            
                           
                             
                           </tbody>
                           <tfoot class="hide-if-no-paging text-center">
                              <tr>
                                <td colspan="6">
                                  <div class="pagination"></div>
                                </td>
                              </tr>
                            </tfoot>

                          </table>
                        </div>
                        <div class="col-md-4">
                          <div id="box_150">
                            <h3>Booked Patients</h3>

                            <ul id="calendar_patients_list" style="list-style: none;padding-left: 0px!important;">
                              
                            </ul>
                          </div>
                        </div>
                      </div>
                        
                     </div>
                  </div>
               </div>
            <!-- search patients Modal -->


               <!-- add patients Modal -->
               <div id="addToQueueModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">

                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Add New Patient</h4>
                        </div>
                        <form action="" method="post">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-6">
                                    First Name
                                    <input type="text" class="ap-input text-left" id="addQueuePatient_firstname" value="" placeholder="First Name" required>
                                    Last Name
                                    <input type="text" class="ap-input text-left" id="addQueuePatient_lastname" value="" placeholder="Last Name" required>
                                    Birthdate
                                    <input type="date" class="ap-input text-left" id="addQueuePatient_birthday" value="" placeholder="Birthday" required>

                                 </div>
                                 <div class="col-md-6">
                                  Age
                                   <input type="number" class="ap-input text-left" id="addQueuePatient_age" value="" placeholder="Age" required>
                                   Address
                                  <input type="text" class="ap-input text-left" id="addQueuePatient_address" value="" placeholder="Address" required>
                                  Contact Number
                                   <input type="number" class="ap-input text-left" id="addQueuePatient_contact" value="" placeholder="Contact Number" required>


                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <a href="#" id="add-patient-queue-button" class="ap-btn ap-btn-primary ap-btn-half">Add to Queue</a>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
            <!-- add patients Modal -->

            <!-- add patients Modal -->
               <div id="addToScheduleModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">

                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Add New Patient</h4>
                        </div>
                        <form action="" method="post">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-6">
                                    First Name
                                    <input type="text" class="ap-input text-left" id="addSchedulePatient_firstname" value="" placeholder="First Name" required>
                                    Last Name
                                    <input type="text" class="ap-input text-left" id="addSchedulePatient_lastname" value="" placeholder="Last Name" required>
                                    Birthdate
                                    <input type="date" class="ap-input text-left" id="addSchedulePatient_birthday" value="" placeholder="Birthday" required>

                                 </div>
                                 <div class="col-md-6">
                                  Age
                                   <input type="number" class="ap-input text-left" id="addSchedulePatient_age" value="" placeholder="Age" required>
                                   Address
                                  <input type="text" class="ap-input text-left" id="addSchedulePatient_address" value="" placeholder="Address" required>
                                  Contact Number
                                   <input type="number" class="ap-input text-left" id="addSchedulePatient_contact" value="" placeholder="Contact Number" required>


                                 </div>
                                 <div class="col-md-12">
                                 Time
                                   <select class="ap-input ap-input-select" id="appointment-modal1-time">
                                       @for($i = 0 ; $i < sizeOf($timeSlots); $i++)
                                          <option value="{{$timeSlots[$i]}}">{{$timeSlots[$i]}}</option>
                                       @endfor
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <a href="#" id="add-patient-schedule-button" class="ap-btn ap-btn-primary ap-btn-half">Book to Schedule</a>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
            <!-- add patients Modal -->


             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
@stop
