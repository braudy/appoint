@extends('app')

@section('title')
  <title>Dashboard</title>
@stop

@section('content')
   @include('header')


     <div class="ap-container container rmv-margin">
     <div id="box_150" id="settings">
       <div id="box_180" class="patient-dashboard-title">
         @if(Auth::check())
            @if(Auth::user()->person->doctor != null)
           <h2>Doctor Dashboard</h2>
           @else
           <h2>Patient Dashboard</h2>
           @endif
         @endif
        </div>

      <div class="container" id="settings-area">
       <div class="row rmv-margin row-flex">
         <div class="col-md-13 rmv-left-padding col-flex">
           <div id="box_180">
              <ul class="ap-tabs-st2 .ap-tabs-st2-animate text-left">
              @if(Auth::check())
                @if(Auth::user()->person->doctor != null)
                <li><a data-toggle="tab" href="#tab3">Activities</a></li>

                <li><a data-toggle="tab" href="#tab4">Patients</a></li>
                @else
                <li><a data-toggle="tab" href="#tab3">Activities</a></li>
                @endif
              @endif  
              </ul>
           </div>
         </div>

      
           
         <div class="col-md-16 rmv-right-padding col-flex">
           <div id="box_180">
             <div class="tab-content">

               <div id="tab4" class="tab-pane fade">
                 <h3 class="rmv-margin">Patients</h3>
                 <i data-toggle="modal" data-target="#add-patient-modal" class="fa fa-plus fa-lg ap-dboard" aria-hidden="true" style="color:green; cursor: pointer;"><p style="display:inline; margin-left: 5px;">Add</p></i>
                 <div class="ap-separator-st2"></div>
                   <div class="ap-container container">
                     <p>
                          Search: <input id="filterx" class="ap-input" type="text">
                          <a href="#clear" class="clear-filter" title="clear filter">[clear]</a>
                        </p>
                        <table class="table table-striped footable toggle-square" data-page-navigation=".pagination"  data-filter-text-only="true" data-filter="#filterx">
                         <thead>
                        
                           <tr>
                             <th>Patient ID</th>
                             <th data-hide="phone">Name</th>
                             <th>Birthdate</th>
                             <th>Address</th>
                           </tr>
                           
                         </thead>
                           <tbody>
                            @for($t = 0; $t < sizeOf($patientsList); $t++)
                            <tr>
                               <td>{{$patientsList[$t]['id']}}</td>
                               <td>{{$patientsList[$t]['firstname']}} {{$patientsList[$t]['lastname']}}</td>
                               <td>{{$patientsList[$t]['birthday']}}</td>
                               <td>{{$patientsList[$t]['address']}}</td>
                            </tr>
                            @endfor
                            
                           
                             
                           </tbody>
                           <tfoot class="hide-if-no-paging text-center">
                              <tr>
                                <td colspan="6">
                                  <div class="pagination"></div>
                                </td>
                              </tr>
                            </tfoot>

                          </table>
                      </div>
                </div>
            
                <!-- insert tab 2 here -->
                <div id="tab3" class="tab-pane fade in active">
                 <h3 class="rmv-margin">Activities</h3>
                 <div class="ap-separator-st2"></div>
                   <div class="ap-container container">
                     
                       
                      <div id="dboard-tab-control">
                    
                        <ul class="ap-tabs">
                           
                           <li><a data-toggle="tab" href="#tab1">Queuer</a></li>
                           <li><a data-toggle="tab" href="#tab2">Scheduler</a></li>
                         </ul>
                      </div>
                      <div class="tab-content">
                        <div id="tab1" class="tab-pane fade in active">
                        @if(Auth::check())
                          @if(Auth::user()->person->doctor != null)

                         <table class="table table-striped footable toggle-square" data-page-navigation=".pagination">
                         <thead>
                           <tr>
                             <th data-toggle="true">Patient</th>
                             <th>Patient Queue #</th>
                             <th data-hide="phone">Actions</th>
                           </tr>
                         </thead>
                         <tbody>
                           @for($i = 0; $i < sizeOf($patientData); $i++)
                           <tr>
                             <td>{{$patientData[$i]['firstname']}} {{$patientData[$i]['lastname']}}</td>
                             <td>{{$patientData[$i]['queueNumber']}}</td>
                             <td>
                             <div style="display: flex;align-items: center;justify-content: center;">
                             @if($i == 0)
                             <form action="{{url('/queue/update')}}" method="post" style="margin-right: 5px">
                             {!!csrf_field()!!}
                             <input type="hidden" name="id" value="{{$patientQueues[$i]['id']}}">
                             <button type="submit" class="btn btn-success">Checkout</button>
                             </form>
                             @endif
                             <form action="{{url('/queue/delete')}}" method="post">
                             {!!csrf_field()!!}
                             <input type="hidden" name="id" value="{{$patientQueues[$i]['id']}}">
                             <button type="submit" class="btn btn-danger">Cancel</button>
                              </form>
                              </div>
                             </td>
                           </tr>
                           @endfor
                         </tbody>
                         <tfoot class="hide-if-no-paging">
                            <tr>
                              <td colspan="5">
                                <div class="pagination pagination-centered hide-if-no-paging"></div>
                              </td>
                            </tr>
                          </tfoot>
                         
                       </table>

                         @else
                         <table class="table table-striped footable toggle-square" data-page-navigation=".pagination">
                         <thead>
                           <tr>
                             <th data-toggle="true">Doctor</th>
                             <th>Your #</th>
                             <th data-hide="phone">Time Slot</th>
                           </tr>
                         </thead>
                          <tbody>
                         @for($j = 0; $j < sizeOf($doctorData);$j++)

                           <tr>
                             <td>{{$doctorData[$j]['firstname']}} {{$doctorData[$j]['lastname']}}</td>
                             <td>{{$doctorData[$j]['queueNumber']}}</td>
                             <td>{{$doctorData[$j]['timeSlot']}}</td>
                           </tr>
                         
                         @endfor
                         </tbody>
                         <tfoot class="hide-if-no-paging">
                            <tr>
                              <td colspan="5">
                                <div class="pagination pagination-centered hide-if-no-paging"></div>
                              </td>
                            </tr>
                          </tfoot>
                         
                       </table>
                         @endif
                       @endif
                       </div>

                    <div id="tab2" class="tab-pane fade">
                        <p>
                          Search: <input id="filter" class="ap-input" type="text">
                          
                        <a href="#clear" class="clear-filter" title="clear filter">[clear]</a>
                      </p>
                        <table class="table table-striped footable toggle-square" data-page-navigation=".pagination"  data-filter-text-only="true" data-filter="#filter">
                         <thead>
                         @if(Auth::check())
                          @if(Auth::user()->person->doctor != null)
                           <tr>
                             <th>Patient</th>
                             <th data-hide="phone">Time</th>
                             <th data-hide="phone">Date</th>
                             <th> Actions </th>
                           </tr>
                           @else
                           <tr>
                             <th>Doctor</th>
                             <th data-hide="phone">Time</th>
                             <th data-hide="phone">Date</th>
                             <th> Actions </th>
                           @endif
                           @endif
                         </thead>
                         <tbody>
                         @if(Auth::check())
                          @if(Auth::user()->person->doctor != null)
                           @for($i=0; $i < sizeOf($doctorSchedData); $i++)
                            <tr>
                             <td>{{$doctorSchedData[$i]['firstname']}} {{$doctorSchedData[$i]['lastname']}}</td>
                             <td>{{$doctorSchedData[$i]['timeSlot']}}</td>
                             <td>{{$doctorSchedData[$i]['date']}}</td>
                             <td>
                             <input type="hidden" value="{{$doctorSchedData[$i]['id']}}" id="hiddenKey">
                             <input type="hidden" id="hidden-sched-id" value="{{$doctorSched[$i]['id']}}">
                             <button data-toggle="modal" data-target="#rescheduleModal" type="button" class="btn btn-success row-hidden {{$doctorSchedData[$i]['id']}} {{$doctorSched[$i]['id']}}">Reschedule</button>
                             {{csrf_field()}}
                             <button id="appointment-modal2-cancel" type="button" class="btn btn-danger {{$doctorSched[$i]['id']}}">Cancel</button>
                             </td>
                           </tr>
                           @endfor
                           @else
                            @for($i=0; $i < sizeOf($patientSchedData); $i++)
                            <tr>
                             <td>{{$patientSchedData[$i]['firstname']}} {{$patientSchedData[$i]['lastname']}}</td>
                             <td>{{$patientSchedData[$i]['timeSlot']}}</td>
                             <td>{{$patientSchedData[$i]['date']}}</td>
                             <td>
                             <button type="button" class="btn btn-danger">Cancel</button>
                             </td>
                           </tr>
                           @endfor
                          @endif
                             
                           
                           @endif
                         
                           
                         </tbody>
                         <tfoot class="hide-if-no-paging text-center">
                            <tr>
                              <td colspan="6">
                                <div class="pagination"></div>
                              </td>
                            </tr>
                          </tfoot>
                          </table>
                         </div>
                      </div>
                      
    
                      </div>
                </div>
                <!-- insert tab 2 here -->
            </div>
</div>
</div>

          
            
         </div>
             
           </div>

               
            </div>
        </div>
  

    </div>
  </div>



<div class="modal fade" id="rescheduleModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reschedule</h4>
        </div>
        <div class="modal-body">
          <div id="set-appointment">
            <div class="row">
              <div class="col-md-12">
                <div class="cal1"></div>
              </div>
            </div>
            </div>
        </div>
        
      </div>
      
    </div>
  </div>

  <!-- appointment Modal -->
               <div id="appointment-modal1" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Set an appoinment</h4>
                        </div>
                        <form action="" method="post">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-12">
                                  {{csrf_field()}}
                                    <input type="hidden" id="appointment-modal2-date">
                                    <input type="hidden" value="{{Auth::user()->person_id}}" id="appointment-modal2-doctor">
                                    <input type="hidden" id="appointment-modal2-patient">
                                    <input type="hidden" id="appointment-modal2-sched-id">
                                    <label for="username">Time</label>
                                    <select id="appointment-modal2-time" class="ap-input ap-input-select">
                                       @for($i = 0 ; $i < sizeOf($timeSlots); $i++)
                                          <option value="{{$timeSlots[$i]}}">{{$timeSlots[$i]}}</option>
                                       @endfor
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <a href="#" id="appointment-modal1-resched" class="ap-btn ap-btn-primary ap-btn-half">Book</a>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
  <!-- appointment Modal -->

  <!-- appointment Modal -->
               <div id="add-patient-modal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-header">
                           <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
                           <h4 class="modal-title">Add New Patient</h4>
                        </div>
                        <form action="" method="post">
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-6">
                                    First Name
                                    <input type="text" class="ap-input text-left" id="addPatient_firstname" value="" placeholder="First Name" required>
                                    Last Name
                                    <input type="text" class="ap-input text-left" id="addPatient_lastname" value="" placeholder="Last Name" required>
                                    Birthdate
                                    <input type="date" class="ap-input text-left" id="addPatient_birthday" value="" placeholder="Birthday" required>

                                 </div>
                                 <div class="col-md-6">
                                  Age
                                   <input type="number" class="ap-input text-left" id="addPatient_age" value="" placeholder="Age" required>
                                   Address
                                  <input type="text" class="ap-input text-left" id="addPatient_address" value="" placeholder="Address" required>
                                  Contact Number
                                   <input type="number" class="ap-input text-left" id="addPatient_contact" value="" placeholder="Contact Number" required>


                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                             <a href="#" id="add-patient-button" class="ap-btn ap-btn-primary ap-btn-half">Add</a>
                          </div>
                        </form>
                     </div>
                  </div>
               </div>
  <!-- appointment Modal -->

@stop
