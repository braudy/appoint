@extends('app')

@section('title')
  <title>Find Doctor</title>
@stop

@section('content')
   @include('header')


   <div class="ap-container container rmv-margin">
     <div id="box_150" id="settings">
       <div id="box_180">
         <h2>Settings</h2>
       </div>

       <div class="container" id="settings-area">
       <div class="row rmv-margin row-flex">
         <div class="col-md-13 rmv-left-padding col-flex">
           <div id="box_180">
              <ul class="ap-tabs-st2 .ap-tabs-st2-animate text-left">
                <li><a data-toggle="tab" href="#tab1">Profile</a></li>
                <li><a data-toggle="tab" href="#tab2">Schedule</a></li>
                <li><a data-toggle="tab" href="#tab3">Account</a></li>
                <li><a data-toggle="tab" href="#tab4">Configurations</a></li>
              </ul>
           </div>
         </div>

         <div class="col-md-16 rmv-right-padding col-flex">
           <div id="box_180">
             <div class="tab-content">

               <div id="tab1" class="tab-pane fade in active">
                 <h3 class="rmv-margin">Profile</h3>
                 <div class="ap-separator-st2"></div>


                 <div class="setting-content">

                  <form action="{{url('/settings/update')}}" method="post">
                    {!! csrf_field() !!}
                   <div class="col-md-6 rmv-left-padding">
                     <label>First Name</label>
                     <input type="text" id="set_fname" name="firstname" class="ap-input" placeholder="First Name" value="{{$data->firstname}}"></input>
                   </div>
                   <div class="col-md-6 rmv-right-padding">
                     <label>Last Name</label>
                    <input type="text" id="set_lname" class="ap-input" name="lastname" placeholder="Last Name (Middle Initial/Name before Last Name)" value="{{$data->lastname}}"></input>
                   </div>

                     <div class="col-md-6 rmv-left-padding">
                       <label>Specialization</label>
                       <select class="ap-input ap-input-select" name="specialization" id="set_specialization">
                          <option class="placeholder" disabled selected hidden>Specialization</option>
                          @if($data->specialization === 'Family Medicine')
                          <option selected value="Family Medicine">Family Medicine</option>
                          @else
                          <option value="Family Medicine">Family Medicine</option>
                          @endif

                          @if($data->specialization === 'General Physician')
                          <option selected value="General Physician">General Physician</option>
                          @else
                          <option value="General Physician">General Physician</option>
                          @endif

                          @if($data->specialization === 'Psychiatry')
                          <option selected value="Psychiatry">Psychiatry</option>
                          @else
                          <option value="Psychiatry">Psychiatry</option>
                          @endif

                          @if($data->specialization === 'Emergency Medicine')
                          <option selected value="Emergency Medicine">Emergency Medicine</option>
                          @else
                          <option value="Emergency Medicine">Emergency Medicine</option>
                          @endif

                          @if($data->specialization === 'Pediatrics')
                          <option selected value="Pediatrics">Pediatrics</option>
                          @else
                          <option value="Pediatrics">Pediatrics</option>
                          @endif

                          @if($data->specialization === 'Obstetrics and Gynecology')
                          <option selected value="Obstetrics and Gynecology">Obstetrics and Gynecology</option>
                          @else
                          <option value="Obstetrics and Gynecology">Obstetrics and Gynecology</option>
                          @endif

                          @if($data->specialization === 'General Surgery')
                          <option selected value="General Surgery">General Surgery</option>
                          @else
                          <option value="General Surgery">General Surgery</option>
                          @endif

                          @if($data->specialization === 'Neurology')
                          <option selected value="Neurology">Neurology</option>
                          @else
                          <option value="Neurology">Neurology</option>
                          @endif

                          @if($data->specialization === 'Orthopedic Surgery')
                          <option selected value="Orthopedic Surgery">Orthopedic Surgery</option>
                          @else
                          <option value="Orthopedic Surgery">Orthopedic Surgery</option>
                          @endif

                          @if($data->specialization === 'Physician Assistant')
                          <option selected value="Physician Assistant">Physician Assistant</option>
                          @else
                          <option value="Physician Assistant">Physician Assistant</option>
                          @endif

                          @if($data->specialization === 'Hematology Oncology')
                          <option selected value="Hematology Oncology">Hematology Oncology</option>
                          @else
                          <option value="Hematology Oncology">Hematology Oncology</option>
                          @endif

                          @if($data->specialization === 'Otolaryngology')
                          <option selected value="Otolaryngology">Otolaryngology</option>
                          @else
                          <option value="Otolaryngology">Otolaryngology</option>
                          @endif

                          @if($data->specialization === 'Cardiology')
                          <option selected value="Cardiology">Cardiology</option>
                          @else
                          <option value="Cardiology">Cardiology</option>
                          @endif

                          @if($data->specialization === 'Gastroenterology')
                          <option selected value="Gastroenterology">Gastroenterology</option>
                          @else
                          <option value="Gastroenterology">Gastroenterology</option>
                          @endif

                          @if($data->specialization === 'Urology')
                          <option selected value="Urology">Urology</option>
                          @else
                          <option value="Urology">Urology</option>
                          @endif

                          @if($data->specialization === 'Pulmonolgy')
                          <option selected value="Pulmonolgy">Pulmonolgy</option>
                          @else
                          <option value="Pulmonolgy">Pulmonolgy</option>
                          @endif

                          @if($data->specialization === 'Dermatology')
                          <option selected value="Dermatology">Dermatology</option>
                          @else
                          <option value="Dermatology">Dermatology</option>
                          @endif

                          @if($data->specialization === 'Geriatrics')
                          <option selected value="Geriatrics">Geriatrics</option>
                          @else
                          <option value="Geriatrics">Geriatrics</option>
                          @endif
                        </select>
                     </div>

                     <div class="col-md-6 rmv-right-padding">
                       <label>Contact Number <span class="ap-input-note">Required</span></label>
                       <input type="text" id="set_contact" name="contact" class="ap-input" placeholder="Contact Number (ex: 09361890167)" value="{{$data->contact}}"></input>
                     </div>

                    


                   <label>Clinic/Hospital Address</label>
                  <input type="text" id="set_caddress" name="clinic_address" class="ap-input" placeholder="Clinic/Hospital Address" value="{{$data->clinic}}"></input>


                   <label>Home Address <span class="ap-input-note">Optional</span></label>
                   <input type="text" id="set_haddress" name="home_address" class="ap-input" placeholder="Home Address" value="{{$data->address}}"></input>

                   <label>Email Address <span class="ap-input-note">Required</span></label>
                   <input type="text" id="set_email" name="email" class="ap-input" placeholder="Email Address" value="{{$data->email_add}}"></input>

                   <label>Description</label>
                   <textarea id="set_description" name="description" class="ap-input ap-textarea" rows="10" cols="40" placeholder="Description here ..">{{$data->description}}</textarea>


                   <div class="ap-spacer"></div>
                   <div class="text-center">
                    <button type="submit" class="ap-btn ap-btn-primary ap-btn-half">Save Changes</button>
                   </div>
                  </form>
                 </div>
               </div>

               <div id="tab2" class="tab-pane fade">
                 <h3 class="rmv-margin">Appointment</h3>
                 <div class="ap-separator-st2"></div>

               <div class="setting-content">
                <table class="table footable">
                  <thead>
                    <tr>
                      
                      <th data-sort-ignore="true" data-hide="phone">AM from</th>
                      <th data-sort-ignore="true" data-hide="phone">AM to</th>
                      <th data-sort-ignore="true" data-hide="phone">PM from</th>
                      <th data-sort-ignore="true" data-hide="phone">PM to</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    <tr>
                      @for ($i = 0; $i < 4; $i++)
                      <td>
                        <select class="ap-input ap-input-select" id="time_{{$i}}" >
                          <option class="placeholder" disabled selected hidden>Pick a time</option>

                          @if($time[$i] == "12:00")
                          <option value="12:00" selected>12:00</option>
                          @else
                          <option value="12:00">12:00</option>
                          @endif
                         
                          @if($time[$i] == "12:30")
                          <option value="12:30" selected>12:30</option>
                          @else
                          <option value="12:30">12:30</option>
                          @endif
                          
                          @if($time[$i] == "1:00")
                          <option value="1:00" selected>1:00</option>
                          @else
                          <option value="1:00">1:00</option>
                          @endif

                          @if($time[$i] == "1:30")
                          <option value="1:30" selected>1:30</option>
                          @else
                          <option value="1:30">1:30</option>
                          @endif

                          @if($time[$i] == "2:00")
                          <option value="2:00" selected>2:00</option>
                          @else
                          <option value="2:00">2:00</option>
                          @endif
                          
                          @if($time[$i] == "2:30")
                          <option value="2:30" selected>2:30</option>
                          @else
                          <option value="2:30">2:30</option>
                          @endif

                          @if($time[$i] == "3:00")
                          <option value="3:00" selected>3:00</option>
                          @else
                          <option value="3:00">3:00</option>
                          @endif

                          @if($time[$i] == "3:30")
                          <option value="3:30" selected>3:30</option>
                          @else
                          <option value="3:30">3:30</option>
                          @endif

                          @if($time[$i] == "4:00")
                          <option value="4:00" selected>4:00</option>
                          @else
                          <option value="4:00">4:00</option>
                          @endif

                          @if($time[$i] == "4:30")
                          <option value="4:30" selected>4:30</option>
                          @else
                          <option value="4:30">4:30</option>
                          @endif

                          @if($time[$i] == "5:00")
                          <option value="5:00" selected>5:00</option>
                          @else
                          <option value="5:00">5:00</option>
                          @endif
                          @if($time[$i] == "5:30")
                          <option value="5:30" selected>5:30</option>
                          @else
                          <option value="5:30">5:30</option>
                          @endif
                        

                         @if($time[$i] == "6:00")
                          <option value="6:00" selected>6:00</option>
                          @else
                          <option value="6:00">6:00</option>
                          @endif
                       
                       @if($time[$i] == "6:30")
                          <option value="6:30" selected>6:30</option>
                          @else
                          <option value="6:30">6:30</option>
                          @endif
                         
                          @if($time[$i] == "7:00")
                          <option value="7:00" selected>7:00</option>
                          @else
                          <option value="7:00">7:00</option>
                          @endif
                       
                         @if($time[$i] == "7:30")
                          <option value="7:30" selected>7:30</option>
                          @else
                          <option value="7:30">7:30</option>
                          @endif
                          

                          @if($time[$i] == "8:00")
                          <option value="8:00" selected>8:00</option>
                          @else
                          <option value="8:00">8:00</option>
                          @endif
                        
                          @if($time[$i] == "8:30")
                          <option value="8:30" selected>8:30</option>
                          @else
                          <option value="8:30">8:30</option>
                          @endif
                         

                         @if($time[$i] == "9:00")
                          <option value="9:00" selected>9:00</option>
                          @else
                          <option value="9:00">9:00</option>
                          @endif

                        @if($time[$i] == "9:30")
                          <option value="9:30" selected>9:30</option>
                          @else
                          <option value="9:30">9:30</option>
                          @endif
                         

                          @if($time[$i] == "10:00")
                          <option value="10:00" selected>10:00</option>
                          @else
                          <option value="10:00">10:00</option>
                          @endif
                        
                          @if($time[$i] == "10:30")
                          <option value="10:30" selected>10:30</option>
                          @else
                          <option value="10:30">10:30</option>
                          @endif
                        

                          @if($time[$i] == "11:00")
                          <option value="11:00" selected>11:00</option>
                          @else
                          <option value="11:00">11:00</option>
                          @endif
                       
                          @if($time[$i] == "11:30")
                          <option value="11:30" selected>11:30</option>
                          @else
                          <option value="11:30">11:30</option>
                          @endif
                         

                      
                       </select>
                     </td>
                    @endfor
                    </tr>

                  </tbody>
                  <tfoot class="hide-if-no-paging text-center">
                     <tr>
                       <td colspan="6">
                         <div class="pagination">
                           
                         </div>
                       </td>
                     </tr>
                   </tfoot>
                </table>

                <div>
                <ul style="list-style-type: none;margin: 0;padding: 0;overflow: hidden;">
                <input type="hidden" id="_tokenF" name="_token" value="{!! csrf_token() !!}">
                <li style="float: left; margin: 0 5px 0 5px;">
                  @if(in_array("8",$days))
                  <label for="x"><input type="checkbox" value="8" checked/> <span class="lelbels">Weekdays only</span></label>
                  @else
                  <label for="x"><input type="checkbox" value="8" /> <span class="lelbels">Weekdays only</span></label>
                  @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;"> 
                @if(in_array("8",$days))
                <label for="x"><input type="checkbox" value="9" checked/> <span class="lelbels">Whole week</span></label>
                @else
                <label for="x"><input type="checkbox" value="9" /> <span class="lelbels">Whole week</span></label>
                @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("0",$days)) 
                <label for="x"><input type="checkbox" value="0" checked/> <span class="lelbels">Sunday</span></label>
                @else
                <label for="x"><input type="checkbox" value="0" /> <span class="lelbels">Sunday</span></label>
                @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("1",$days))
                <label for="x"><input type="checkbox" value="1" checked/> <span class="lelbels">Monday</span></label>
                @else
                <label for="x"><input type="checkbox" value="1" /> <span class="lelbels">Monday</span></label>
                @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("2",$days))
                <label for="x"><input type="checkbox" value="2" checked/> <span class="lelbels">Tuesday</span></label>
                @else
                <label for="x"><input type="checkbox" value="2" /> <span class="lelbels">Tuesday</span></label>
                @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("3",$days))
                <label for="x"><input type="checkbox" value="3" checked/> <span class="lelbels">Wednesday</span></label>
                @else
                <label for="x"><input type="checkbox" value="3" /> <span class="lelbels">Wednesday</span></label>
                @endif
                </li>

                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("4",$days))
                <label for="x"><input type="checkbox" value="4" checked/> <span class="lelbels">Thursday</span></label>
                @else
                <label for="x"><input type="checkbox" value="4" /> <span class="lelbels">Thursday</span></label>
                @endif
                </li>
                 
                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("5",$days))
                <label for="x"><input type="checkbox" value="5" checked/> <span class="lelbels">Friday</span></label>
                @else
                <label for="x"><input type="checkbox" value="5" /> <span class="lelbels">Friday</span></label>
                @endif
                </li>
                 
                <li style="float: left; margin: 0 5px 0 5px;">
                @if(in_array("6",$days))
                <label for="x"><input type="checkbox" value="6" checked/> <span class="lelbels">Saturday</span></label>
                @else
                <label for="x"><input type="checkbox" value="6" /> <span class="lelbels">Saturday</span></label>
                @endif
                </li>

                </div>
              </ul>
                <div class="text-center">
                  <button style="margin-top: 20px" class="ap-btn ap-btn-primary ap-btn-half" id="saveScheduleChanges">Save Changes</button>
                 </div>


               </div>
               </div>

               <div id="tab3" class="tab-pane fade">
                 <h3 class="rmv-margin">Account</h3>
                 <div class="ap-separator-st2"></div>
                 <label>Username</label>
                 <input type="text" class="ap-input" placeholder="Username" value="{{$data->username}}"></input>
                 <label>Password</label>
                 <input type="password" class="ap-input" placeholder="Password"></input>

                 <div class="ap-spacer"></div>
                 <div class="text-center">
                  <button class="ap-btn ap-btn-primary ap-btn-half">Save Changes</button>
                 </div>
               </div>

               <div id="tab4" class="tab-pane fade">
                 <h3 class="rmv-margin">Configuration</h3>
                 <div class="ap-separator-st2"></div>
                 
                 <label>Accomodation Slot Limit:</label><br>
                 <div class="col-md-6 rmv-left-padding">
                   <label>AM</label><br>
                   <input name="amLimit" id="amLimit" type="number" class="ap-input ap-input-quarter-max" placeholder="Input numbers only .." value="{{$settings->amLimit}}"></input>
                 </div>
                 <div class="col-md-6 rmv-right-padding">
                   <label>PM</label><br>
                  <input name="pmLimit" id="pmLimit" type="number" class="ap-input ap-input-quarter-max" placeholder="Input numbers only .." value="{{$settings->pmLimit}}"></input>
                 </div>

                 <!-- <div class="ap-input-note ap-input-note-2x rmv-left-padding">*If no checkbox is selected, the fields will be good for the day only..</div>
                 <input type="checkbox" name="name" value="Week"> <label>Apply this to whole week</label></input><br>
                 <input type="checkbox" name="name" value="Auto"> <label>Let the system set the slot limit automatically</label></input> -->

                 <div class="text-center">
                  <button class="ap-btn ap-btn-primary ap-btn-half" style="margin-top:20px" id="saveConfig">Save Changes</button>
                 </div>
             

               </div>

             </div>
          </div>
        </div>
        </div>
      </div>

    </div>
  </div>

@stop
