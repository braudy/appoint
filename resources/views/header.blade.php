<div id="header">
    <div class="ap-header navbar-fixed-top">
        <div class="row">
            <div class="col-md-12">
                <a href="/" class="no-decoration nav-logo">MedSched</a>
                <div class="header-toggler">
                    <button type="button" class="ap-btn ap-btn-primary">
                        <i class="fa fa-bars" aria-hidden="true"></i>

                    </button>
                </div>
                
                    @if(Auth::check())
                    <div class="ap-dboard-header">
                        <ul class="ap-dboard-ul">
                            <li>
                                <a href="{{url('/search')}}"><i class="fa fa-search ap-dboard"
                                               aria-hidden="true"><p>Find Doctor</p></i></a>
                            </li>
                        
                            <li>
                                <a href="/dashboard"><i class="fa fa-home" data-target="#account-menu"
                                               aria-hidden="true"><p>Dashboard</p></i></a>
                            </li>

                            @if(Auth::user()->person->doctor != null)
                            <input type="hidden" id="secretLoveSong" value="true">
                            <li>
                                <a href="/profile/{{Auth::user()->id}}/{{Auth::user()->person()->first()->doctor()->first()->slug}}"><i class="fa fa-home" data-target="#account-menu"
                                               aria-hidden="true"><p>Profile</p></i></a>
                            </li>
                            <li>
                                <a href="/settings/{{Auth::user()->id}}/{{Auth::user()->person()->first()->doctor()->first()->slug}}"><i class="fa fa-cog" data-target="#account-menu"
                                               aria-hidden="true"><p>Settings</p></i></a>
                            </li>
                            @else
                            <input type="hidden" id="secretLoveSong" value="false">
                            @endif

                            <li>
                                <a href="/logout"><i class="fa fa-sign-in" data-target="#account-menu"
                                               aria-hidden="true"><p>Logout</p></i></a>
                            </li>
                        </ul>
                    @else
                    <div class="ap-dboard-header">
                        <ul class="ap-dboard-ul">
                            <li>
                                <a href="/login"><i class="fa fa-sign-in"><p>Login</p></i></a>
                            </li>
                            <li>
                                <a href="/register"><i class="fa fa-user-plus" aria-hidden="true"><p>Register</p>
                                    </i></a>
                            </li>
                        </ul>

                    @endif
                </div>
                <div id="notifs" class="ap-dboard-collapsable-notifs">
                    <div class="notification-area">
                        <ul>
                        
                        </ul>
                    </div>
                </div>
                <div id="account-menu" class="ap-dboard-collapsable-menu">
                    <ul>
                      @if(Auth::check())
                        @if(Auth::user()->person->doctor != null)
                        
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="/dashboard">Dashboard</a></li>

                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="/profile/{{Auth::user()->id}}/{{Auth::user()->person()->first()->doctor()->first()->slug}}">Profile</a></li>
                        <li><i class="fa fa-cog" aria-hidden="true"></i><a href="/settings/{{Auth::user()->id}}/{{Auth::user()->person()->first()->doctor()->first()->slug}}">Settings</a></li>

                        <li><i class="fa fa-sign-out" aria-hidden="true"></i><a href="/logout">Logout</a></li>
                        @else
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="/dashboard">Dashboard</a></li>
                        <li><i class="fa fa-sign-out" aria-hidden="true"></i><a href="/logout">Logout</a></li>
                        @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
