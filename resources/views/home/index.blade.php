@extends('app')

@section('title')
  <title>Home</title>
@stop

@section('content')
   @include('header')
   <div class="ap-container container">
     <div id="box_150">
         <div id="home-banner">

              <img src="images/image3.jpg">
                <div class="strip"></div>
                <h5>Take a number. Get a Schedule. <br> Be Reminded. Be Served.</h5>
                <h4>Being healthy is now less hassle</h4>
         </div>
         <div class="ap-separator-st2"></div>
     </div>
 </div>

 <div id="find">
     <div id="box_100">
       <div class="box-title"> Find a Doctor </div>
       <div class="box-content">
         <div class="row">
             <div class="col-md-15">
               <div class="ap-icon-input">
                 <i class="fa fa-search ap-icon"></i>
                 <input type="text" class="ap-input" name="name" placeholder="Search a keyword ..">
               </div>
             </div>

             <div class="col-md-18">
               <div class="ap-icon-input">
                 <i class="fa fa-user-md"></i>
                 <select class="ap-input ap-input-select" name="state" style="color: grey;">
                   <option class="placeholder" disabled="" selected="" hidden="" value="Specialization">Specialization</option>
                   <option value="family-medicine">Family medicine</option>
                  <option value="General Physician">General Physician</option>
                  <option value="psychiatry">Psychiatry</option>
                  <option value="emergency-medicine">Emergency medicine</option>
                  <option value="pediatrics">Pediatrics</option>
                  <option value="obstetrics-and-gynecology">Obstetrics and gynecology</option>
                  <option value="general-surgery">General surgery</option>
                  <option value="neurology">Neurology</option>
                  <option value="orthopedic surgery">Orthopedic surgery</option>
                  <option value="physician assistant">Physician assistant</option>
                  <option value="hematology-oncology">Hematology and/or oncology</option>
                  <option value="otolaryngology">Otolaryngology</option>
                  <option value="cardiology">Cardiology</option>
                  <option value="gastroenterology">Gastroenterology</option>
                  <option value="urology">Urology</option>
                  <option value="pulmonolgy ">Pulmonolgy </option>
                  <option value="dermatology">Dermatology</option>
                  <option value="geriatrics">Geriatrics</option>
                 </select>
               </div>
             </div>

               <div class="col-md-17">
                 <input type="button" class="ap-input ap-btn ap-btn-default" value="Search">
               </div>
           </div>
          </div>
       </div>
     </div>


 <div id="what-you-can-do">


     <div class="container">
       <div class="row text-center">
           <div class="title-area">
               <h2 class="title">Services</h2>
               <!-- SERVICES ni DIRI -->
               <span class="title-line"></span>
           </div>
       </div>
         <!-- 1st row -->
         <div class="row text-center">
             <div class="col-md-3 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <!-- <div class="iconSize80 websiteIcons iconBottomPadding iconHealth center-block"> </div> -->

                     <img class="website-icons" src="images/findadoctor.png">
                     <h3>Find a Doctor</h3>
                     <p> Looking for a specialized doctor in Iligan City? Find the perfect doctor for you in just one click of a button. </p>
                     <!-- </div> -->
                 </div>
             </div>

             <div class="col-md-3 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <img class="website-icons" src="images/calendarsearch.png">
                     <h3>View Schedule</h3>
                     <p> Real-time access to doctors' schedule.</p>
                 </div>
             </div>

             <div class="col-md-3 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <img class="website-icons" src="images/bookonline.png">
                     <h3>Book Appointment</h3>
                     <p> Instantly book in ADVANCE your appointment online.</p>
                 </div>
             </div>

             <div class="col-md-3 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <img class="website-icons" src="images/queue23.png">
                     <h3>Online Queuing</h3>
                     <p> Tired of waiting for hours? Say goodbye to lining up and get your priority number online.</p>
                 </div>
             </div>

         </div>
     </div>




     <div class="container">
       <div class="row text-center">
           <div class="title-area">
               <h2 class="title">Features</h2>
               <!-- FEATURES ni DIRI -->
               <span class="title-line"></span>
           </div>
       </div>

         <!-- 2nd row -->
         <div class="row text-center">
             <div class="col-md-6 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <img class="website-icons" src="images/sms.png">
                     <h3>SMS Reminders</h3>
                     <p> Forgetting scheduled appointments is not a problem. SMS reminders is there for you. </p>
                 </div>
             </div>

             <div class="col-md-6 col-sm-6">
                 <div id="box_200" class="bg bg_animate">
                     <img class="website-icons" src="images/ratings.png">
                     <h3>Ratings and Reviews</h3>
                     <p>Rate your doctor's service and post your comments & suggestions. </p>
                 </div>
             </div>

         </div>
     </div>
 </div>


@stop
