<!-- Modals -->
<!-- Register -->
<div id="RegisterModal" class="modal fade" role="dialog" ng-controller="RegistrationController as registrationCtrl">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
            <h4 class="modal-title">Register As</h4>
         </div>
         <div class="modal-body">
            <div class="row">
               <div class="col-md-6">
                  <a href="#"  class="ap-btn ap-btn-primary" ng-click="tab=1">Patient</a>
               </div>
               <div class="col-md-6">
                 <a href="#"  class="ap-btn ap-btn-primary" ng-click="tab=2">Doctor</a>
               </div>
            </div>
            <hr>

            <form name="registration_form" id="registration_form" novalidate>
               <div class="tab-content">
                  <div id="form1" class="tab-pane fade in active">

                     <div id="form" class="form-group">
                        <section ng-show="tab <= 2">
                           <label for="name">Name</label>
                           <div id="name" class="row">
                              <div class="col-md-4 rmv-right-padding">
                                 <input type="text" name="firstname" ng-model="registration.firstname" class="ap-input maximize" placeholder="First" ng-minlength="4" required>
                              </div>
                              <div class="col-md-4">
                                 <input type="text" name="middlename" ng-model="registration.middlename" class="ap-input maximize" placeholder="Middle" ng-minlength="1" required>
                              </div>
                              <div class="col-md-4 rmv-left-padding">
                                 <input type="text" name="lastname" ng-model="registration.lastname" class="ap-input maximize" placeholder="Last" ng-minlength="4" required>
                              </div>
                           </div>
                           <br>
                           <div class="row">
                              <div class="col-md-6 rmv-right-padding">
                                 <label for="name">Gender</label>
                                 <select class="ap-input ap-input-select" name="gender" ng-model="registration.gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                 </select>
                              </div>
                              <div class="col-md-6">
                                 <label for="birthdate">Birthdate</label>
                                 <input type="date" name="birthdate" class="ap-input" placeholder="yyyy-MM-dd" min="1900-01-01" max="2016-12-31" ng-model="registration.birthdate" required>
                              </div>
                           </div>
                           <br>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="address">Address</label>
                                 <input type="text" name="address" class="ap-input" placeholder="Address" ng-model="registration.address" ng-minlength="4" required>
                              </div>
                           </div>
                              <br>
                           <div class="row">
                              <div class="col-md-12">
                                 <label for="religion">Religion</label>
                                 <select ng-model="registration.religion" class="ap-input ap-input-select" ng-options='rel for rel in religions'></select>
                              </div>
                           </div>
                        </section>
                        <br ng-show="tab===2">
                        <div id="doctor_add" class="row" ng-show="tab===2">
                           <div class="col-md-12">
                              <label for="specialization">Specialization</label>
                              <select ng-model="registration.specialzation" class="ap-input ap-input-select" ng-options='spc for spc in specialzations'></select>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div id="form2" class="tab-pane fade">
                     <div class="form-group">
                        <div class="row">
                           <div class="col-md-12">
                              <label for="name">Username</label>
                              <input type="text" name="username" class="ap-input maximize" placeholder="Username" ng-model="registration.username" ng-minlength="4" required>
                           </div>
                         </div>
                           <br>
                         <div class="row">
                           <div class="col-md-12">
                              <label for="name">Password</label>
                              <input type="password" name="password" class="ap-input maximize" placeholder="Password" ng-model="registration.password" ng-minlength="4" required>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer" ng-show="tab != null">
              <a data-toggle="tab" class="ap-btn ap-btn-standard" href="#form1" ng-click="d_tab=0">Previous</a>
              <a ng-hide="d_tab==1" data-toggle="tab" class="ap-btn ap-btn-standard" href="#form2" ng-click="d_tab=1">Next</a>
              <a ng-show="d_tab==1" class="ap-btn ap-btn-standard" ng-disabled="registration_form.$invalid">Submit</a>
           </div>
            </form>
      </div>
   </div>
</div>
<!-- Register Modal end -->

<!-- Login Modal -->
<div id="LoginModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-lg fa-times"></i></button>
            <h4 class="modal-title">Login</h4>
         </div>
         <form action="" method="post">
            <div class="modal-body">
               <div class="row">
                  <div class="col-md-12">
                     <label for="username">Username</label>
                     <input type="text" name="username" class="ap-input" placeholder="Username">
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-12">
                     <label for="password">Password</label>
                     <input type="password" name="password" class="ap-input" placeholder="Password">
                  </div>
               </div>
            </div>
            <div class="modal-footer">
              <a href="/profile" class="ap-btn ap-btn-primary ap-btn-half">Login</a>
           </div>
         </form>
      </div>
   </div>
</div>
<!-- Login Modal end -->
