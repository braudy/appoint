@extends('app')

@section('title')
  <title>Dashboard</title>
@stop

@section('content')
   @include('header')

    <form action="{{url('/sendtest')}}" method="post">
     <input type="text" name="message">
     {!! csrf_field() !!}
     <button type="submit">Send</button>
   </form>
@stop
